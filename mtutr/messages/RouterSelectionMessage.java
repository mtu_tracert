/*    */ package mtutr.messages;
/*    */ 
/*    */ /*    */ import java.awt.Color;
/*    */ import java.awt.FlowLayout;
/*    */ import java.awt.Font;
/*    */ import java.awt.event.ActionEvent;
/*    */ import java.awt.event.ActionListener;
/*    */ import javax.swing.JButton;
/*    */ import javax.swing.JLabel;
import javax.swing.JPanel;

import mtutr.MainDrawBoard;
/*    */ 
/*    */ public class RouterSelectionMessage extends JPanel
/*    */ {
/*    */   private static final long serialVersionUID = 1L;
/* 17 */   private static RouterSelectionMessage instance = null;
/*    */ 
/*    */   public static RouterSelectionMessage getInstance(MainDrawBoard board) {
/* 20 */     if (instance == null) {
/* 21 */       instance = new RouterSelectionMessage(board);
/*    */     }
/* 23 */     return instance;
/*    */   }
/*    */ 
/*    */   private RouterSelectionMessage(MainDrawBoard theBoard) {
/* 27 */     FlowLayout fl = new FlowLayout();
/* 28 */     fl.setAlignment(0);
/* 29 */     setLayout(fl);
/*    */ 
/* 31 */     setOpaque(false);
/*    */ 
/* 33 */     JLabel label = new JLabel("Select network's default gateway                ");
/* 34 */     label.setHorizontalAlignment(10);
/* 35 */     label.setFont(new Font("Tahoma", 0, 11));
/* 36 */     add(label);
/* 37 */     JButton button = new JButton("Done");
/* 38 */     button.setBackground(new Color(188, 213, 254));
/* 39 */     button.setFont(new Font("Tahoma", 0, 11));
/* 40 */     button.addActionListener(new ActionListener() {
/*    */       public void actionPerformed(ActionEvent event) {
/* 42 */         MainDrawBoard.staticInstance.exitRouterSelectionMode();
/*    */       }
/*    */     });
/* 45 */     add(button);
/*    */   }
/*    */ }

/* Location:           C:\Users\MT\Desktop\mtutr.jar
 * Qualified Name:     mtutr.messages.RouterSelectionMessage
 * JD-Core Version:    0.5.4
 */
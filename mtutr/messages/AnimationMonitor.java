package mtutr.messages;

import java.awt.Color;
import java.awt.Component;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Iterator;
import java.util.Map;

import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JToggleButton;

import mtutr.objects.Datagram;
import mtutr.objects.Network;
import mtutr.objects.RouteTestResult;
import mtutr.objects.Router;

public class AnimationMonitor extends JPanel implements ActionListener {
	private static final long serialVersionUID = 1L;
	private static AnimationMonitor instance = null;
	private final JLabel MTULabel;
	private final JLabel ttlLabel;
	private final JLabel errLabel;
	private final JToggleButton stopAnimn;
	public boolean shouldStop = false;
	public JLabel legendLabel;

	public static AnimationMonitor getInstance() {
		if (instance == null) {
			instance = new AnimationMonitor();
		}
		return instance;
	}

	public void updateTTL(int ttl) {
		this.ttlLabel.setText("TTL: " + ttl + "   ");
		this.ttlLabel.updateUI();
	}

	public void updateMTU(int mtu) {
		this.MTULabel.setText("Datagram size: " + mtu + "   ");
	}

	public void updateMTUs(Map fragments) {
		StringBuffer sb = new StringBuffer();
		sb
				.append("<html>Datagram fragments: fragment size(num of fragments)&nbsp;&nbsp;&nbsp;<table border=\"1\" CELLSPACING=\"0\"><tr>");
		Iterator it = fragments.keySet().iterator();
		while (it.hasNext()) {
			Object o = it.next();
			sb.append("<td>").append(o).append(" (x").append(fragments.get(o))
					.append(")</td>");
		}
		sb.append("</tr></table></html>");
		this.MTULabel.setText(sb.toString());
	}

	public void setError(String err) {
		this.errLabel.setText(err);
		this.errLabel.updateUI();
	}

	public void enableStop() {
		this.shouldStop = false;
		this.stopAnimn.setVisible(true);
		this.stopAnimn.setText("Stop Simulation");
		this.stopAnimn.setEnabled(true);
		this.stopAnimn.setSelected(false);
		this.stopAnimn.setForeground(Color.black);
		repaint();
	}

	public void enableStopping() {
		this.shouldStop = true;
		this.stopAnimn.setVisible(true);
		this.stopAnimn.setText("Stopping Simulation...");
		this.stopAnimn.setSelected(true);
		repaint();
	}

	public void hideStopSim() {
		this.stopAnimn.setVisible(false);
		repaint();
	}

	public void setLegend(Component obj, Datagram d) {
		if (obj == null) {
			this.legendLabel.setText("");
			return;
		}

		String str = "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;";

		if (obj instanceof Router) {
			RouteTestResult rtr = ((Router) obj).routingTable.findMatch(d
					.getTarget().getIP());
			if (rtr == null) {
				this.legendLabel.setText("");
				return;
			}

			str = str + "Current Router: matched routing entry<br>";
			str = str
					+ "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;"
					+ rtr.rte.toString();
			this.legendLabel.setText("<HTML>" + str + "</HTML>");
		} else if (obj instanceof Network) {
			str = str + "Current Network<br>";
			str = str
					+ "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;MTU size:&nbsp;&nbsp;"
					+ ((Network) obj).getMTU();
			this.legendLabel.setText("<HTML>" + str + "</HTML>");
		}
	}

	private AnimationMonitor() {
		FlowLayout fl = new FlowLayout();
		fl.setAlignment(0);
		setLayout(fl);

		setOpaque(false);

		this.MTULabel = new JLabel("");
		this.MTULabel.setHorizontalAlignment(10);
		this.MTULabel.setFont(new Font("Tahoma", 0, 11));
		add(this.MTULabel);

		this.ttlLabel = new JLabel("");
		this.ttlLabel.setHorizontalAlignment(10);
		this.ttlLabel.setFont(new Font("Tahoma", 0, 11));
		add(this.ttlLabel);

		this.errLabel = new JLabel("");
		this.errLabel.setHorizontalAlignment(10);
		this.errLabel.setFont(new Font("Tahoma", 0, 11));
		this.errLabel.setForeground(Color.RED);
		add(this.errLabel);

		this.stopAnimn = new JToggleButton("Stop Simulation");
		this.stopAnimn.setHorizontalAlignment(10);
		this.stopAnimn.setFont(new Font("Tahoma", 0, 11));
		this.stopAnimn.setBackground(new Color(188, 213, 254));
		this.stopAnimn.addActionListener(this);
		add(this.stopAnimn);

		this.legendLabel = new JLabel("");
		this.legendLabel.setHorizontalAlignment(10);
		this.legendLabel.setFont(new Font("Tahoma", 0, 11));
		add(this.legendLabel);

		repaint();
	}

	public void actionPerformed(ActionEvent arg0) {
		enableStopping();
	}
}

/*
 * Location: C:\Users\MT\Desktop\mtutr.jar Qualified Name:
 * mtutr.messages.AnimationMonitor JD-Core Version: 0.5.4
 */
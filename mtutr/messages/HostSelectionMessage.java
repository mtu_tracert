/*    */ package mtutr.messages;
/*    */ 
/*    */ /*    */ import java.awt.Color;
/*    */ import java.awt.FlowLayout;
/*    */ import java.awt.Font;
/*    */ import java.awt.event.ActionEvent;
/*    */ import java.awt.event.ActionListener;
/*    */ import javax.swing.JButton;
/*    */ import javax.swing.JLabel;
import javax.swing.JPanel;

import mtutr.MainDrawBoard;
/*    */ 
/*    */ public class HostSelectionMessage extends JPanel
/*    */ {
/*    */   private static final long serialVersionUID = 1L;
/* 17 */   private static HostSelectionMessage instance = null;
/* 18 */   private static JLabel label = null;
/*    */ 
/*    */   public static HostSelectionMessage getInstance(boolean source) {
/* 21 */     if (instance == null) {
/* 22 */       instance = new HostSelectionMessage(source);
/*    */     }
/*    */ 
/* 25 */     label.setText("<html>Select <b>" + ((source) ? "source" : "target") + "</b> host of datagram      <html>");
/*    */ 
/* 27 */     return instance;
/*    */   }
/*    */ 
/*    */   private HostSelectionMessage(boolean source) {
/* 31 */     FlowLayout fl = new FlowLayout();
/* 32 */     fl.setAlignment(0);
/* 33 */     setLayout(fl);
/*    */ 
/* 35 */     setOpaque(false);
/*    */ 
/* 37 */     label = new JLabel("<html>Select <b>" + ((source) ? "source" : "target") + "</b> host of datagram      <html>");
/* 38 */     label.setHorizontalAlignment(10);
/* 39 */     label.setFont(new Font("Tahoma", 0, 11));
/* 40 */     add(label);
/* 41 */     JButton button = new JButton("Done");
/* 42 */     button.setBackground(new Color(188, 213, 254));
/* 43 */     button.setFont(new Font("Tahoma", 0, 11));
/* 44 */     button.addActionListener(new ActionListener() {
/*    */       public void actionPerformed(ActionEvent event) {
/* 46 */         MainDrawBoard.staticInstance.exitHostSelectionMode();
/*    */       }
/*    */     });
/* 49 */     add(button);
/*    */   }
/*    */ }

/* Location:           C:\Users\MT\Desktop\mtutr.jar
 * Qualified Name:     mtutr.messages.HostSelectionMessage
 * JD-Core Version:    0.5.4
 */
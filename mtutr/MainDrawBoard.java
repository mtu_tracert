package mtutr;

import java.awt.Color;
import java.awt.Component;
import java.awt.Cursor;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.Toolkit;
import java.io.BufferedInputStream;
import java.io.IOException;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.ListIterator;

import javax.swing.BorderFactory;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JMenu;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;

import mtutr.listeners.MainMouseListener;
import mtutr.listeners.MenuListener;
import mtutr.listeners.NetworkPlacementListener;
import mtutr.listeners.NetworkTypeButtonListener;
import mtutr.listeners.SelectionMouseListener;
import mtutr.messages.HostSelectionMessage;
import mtutr.messages.NetworkSelectionMessage;
import mtutr.messages.RouterSelectionMessage;
import mtutr.objects.Datagram;
import mtutr.objects.Host;
import mtutr.objects.Network;
import mtutr.objects.Router;
import mtutr.windows.ChooseDefaultGateway;

public class MainDrawBoard extends JPanel {
	public static MainDrawBoard staticInstance = null;
	public MtuTr ipKit = null;
	int MAX_IMAGE_SIZE = 3868;
	byte[] buf = new byte[this.MAX_IMAGE_SIZE];
	private static final long serialVersionUID = 1L;
	private ImageIcon networkImage = null;
	private ImageIcon hostImage = null;
	private ImageIcon envelopeImage = null;
	private ImageIcon fragImage = null;
	private ImageIcon routerImage = null;

	int x = 10;
	int y = 20;

	public JPopupMenu main_menu = null;
	public JPopupMenu network_menu = null;
	public JPopupMenu host_menu = null;
	public JPopupMenu router_menu = null;
	public JPopupMenu datagram_menu = null;

	public MenuListener menuListener = null;
	public NetworkPlacementListener networkPlacementListener = null;
	public MainMouseListener mouseListener = null;
	public SelectionMouseListener selMouseListener = null;
	public JPanel messagePanel;
	public List networks = new LinkedList();
	public List routers = new LinkedList();
	public List datagrams = new LinkedList();
	public JMenuItem addHost = null;
	public JMenuItem sendDatagram = null;
	private Datagram datagram;
	public boolean sourceAndTarget = false;
	public boolean firstClick;
	private boolean isIPv6;

	public JButton btnIPv6;
	public JButton btnIPv4;

	public MainDrawBoard(JPanel theMsgPanel, MtuTr frame) {
		staticInstance = this;
		this.messagePanel = theMsgPanel;

		this.messagePanel.setBackground(new Color(222, 231, 254));
		this.messagePanel.setVisible(true);
		((FlowLayout) this.messagePanel.getLayout()).setAlignment(0);
		this.ipKit = frame;

		if (frame.firstClick) {
			this.firstClick = frame.firstClick;
			
			this.btnIPv6 = new JButton("IPv6");
			Font font = new Font("Arial", 1, 40);
			this.btnIPv6.setFont(font);
			this.btnIPv6.setSize(this.btnIPv6.getPreferredSize());
			this.btnIPv6.setLocation(120, 250);
			this.btnIPv6.addActionListener(new NetworkTypeButtonListener(this, true));

			this.btnIPv4 = new JButton("IPv4");
			this.btnIPv4.setFont(font);
			this.btnIPv4.setSize(this.btnIPv4.getPreferredSize());
			this.btnIPv4.setLocation(350, 250);
			this.btnIPv4.addActionListener(new NetworkTypeButtonListener(this, false));

			add(this.btnIPv6);
			add(this.btnIPv4);
		}

		this.mouseListener = new MainMouseListener(this);
		this.selMouseListener = new SelectionMouseListener(this);

		addMouseListener(this.mouseListener);
		addMouseMotionListener(this.mouseListener);

		this.menuListener = new MenuListener(this);
		this.main_menu = new JPopupMenu("Main Menu");
		JMenuItem m_item = new JMenuItem("Add Network");
		m_item.addActionListener(this.menuListener);
		this.main_menu.add(m_item);
		m_item = new JMenuItem("Add Router");
		m_item.addActionListener(this.menuListener);
		this.main_menu.add(m_item);
		m_item = new JMenuItem("Add Datagram");
		m_item.addActionListener(this.menuListener);
		this.main_menu.add(m_item);
		this.main_menu.addSeparator();

		this.networkPlacementListener = new NetworkPlacementListener(this,
				this.menuListener);
		JMenu place_networks = new JMenu("Place Networks");
		m_item = new JMenuItem("Star");
		m_item.addActionListener(this.networkPlacementListener);
		place_networks.add(m_item);
		m_item = new JMenuItem("Loop");
		m_item.addActionListener(this.networkPlacementListener);
		place_networks.add(m_item);
		m_item = new JMenuItem("Long");
		m_item.addActionListener(this.networkPlacementListener);
		place_networks.add(m_item);
		this.main_menu.add(place_networks);

		this.main_menu.addSeparator();
		m_item = new JMenuItem("Remove All");
		m_item.addActionListener(this.menuListener);
		this.main_menu.add(m_item);

		this.network_menu = new JPopupMenu("Network menu");
		this.addHost = new JMenuItem("Add Host");
		this.addHost.addActionListener(this.menuListener);
		this.network_menu.add(this.addHost);
		this.network_menu.addSeparator();
		m_item = new JMenuItem("Set Default Gateway");
		m_item.addActionListener(this.menuListener);
		this.network_menu.add(m_item);
		this.network_menu.addSeparator();
		m_item = new JMenuItem("Edit Network");
		m_item.addActionListener(this.menuListener);
		this.network_menu.add(m_item);
		m_item = new JMenuItem("Remove Network");
		m_item.addActionListener(this.menuListener);
		this.network_menu.add(m_item);

		this.router_menu = new JPopupMenu("Router menu");
		m_item = new JMenuItem("Connect to Networks");
		m_item.addActionListener(this.menuListener);
		this.router_menu.add(m_item);
		this.router_menu.addSeparator();
		m_item = new JMenuItem("Edit Routing Table");
		m_item.addActionListener(this.menuListener);
		this.router_menu.add(m_item);
		m_item = new JMenuItem("Remove Router");
		m_item.addActionListener(this.menuListener);
		this.router_menu.add(m_item);

		this.host_menu = new JPopupMenu("Host menu");
		m_item = new JMenuItem("Remove Host");
		m_item.addActionListener(this.menuListener);
		this.host_menu.add(m_item);

		this.datagram_menu = new JPopupMenu("Datagram menu");
		this.sendDatagram = new JMenuItem("Send Datagram");
		this.sendDatagram.addActionListener(this.menuListener);
		this.datagram_menu.add(this.sendDatagram);
		this.datagram_menu.addSeparator();
		m_item = new JMenuItem("Set Source & Target");
		m_item.addActionListener(this.menuListener);
		this.datagram_menu.add(m_item);
		m_item = new JMenuItem("Set Source Only");
		m_item.addActionListener(this.menuListener);
		this.datagram_menu.add(m_item);
		m_item = new JMenuItem("Set Target Only");
		m_item.addActionListener(this.menuListener);
		this.datagram_menu.add(m_item);
		this.datagram_menu.addSeparator();
		m_item = new JMenuItem("Edit Datagram");
		m_item.addActionListener(this.menuListener);
		this.datagram_menu.add(m_item);
		m_item = new JMenuItem("Remove Datagram");
		m_item.addActionListener(this.menuListener);
		this.datagram_menu.add(m_item);
	}

	@Override
	protected void paintComponent(Graphics graphics) {
		super.paintComponent(graphics);
		Iterator it = this.networks.iterator();
		Network net = null;
		Host host = null;
		while (it.hasNext()) {
			net = (Network) it.next();
			Iterator hIt = net.hosts.iterator();
			while (hIt.hasNext()) {
				host = (Host) hIt.next();
				graphics.drawLine(getCenterX(net), getCenterY(net),
						getCenterX(host), getCenterY(host));
				graphics.drawString(host.getIP().getIPStr(),
						getCenterX(host) - 23, getCenterY(host) + 25);
			}

		}

		it = this.routers.iterator();
		while (it.hasNext()) {
			Router router = (Router) it.next();
			Iterator nIt = router.networks.iterator();
			while (nIt.hasNext()) {
				net = (Network) nIt.next();
				graphics.drawLine(getCenterX(net), getCenterY(net),
						getCenterX(router), getCenterY(router));
				graphics.drawString(router.getIP(net).getIPStr(),
						getCenterX(net)
								+ (getCenterX(router) - getCenterX(net)) / 5
								* 4, getCenterY(net)
								+ (getCenterY(router) - getCenterY(net)) / 5
								* 4);

				if ((net.defaultGateway != null)
						&& (net.defaultGateway.equals(router)))
					graphics.drawString("d", getCenterX(net)
							+ (getCenterX(router) - getCenterX(net)) / 2,
							getCenterY(net)
									+ (getCenterY(router) - getCenterY(net))
									/ 2);
			}
		}
	}

	private int getCenterX(Component c) {
		return c.getX() + c.getWidth() / 2;
	}

	private int getCenterY(Component c) {
		return c.getY() + c.getHeight() / 2;
	}

	public void exitSelectionMode() {
		removeMouseListener(this.selMouseListener);
		addMouseListener(this.mouseListener);
		setCursor(new Cursor(0));
		Iterator it = this.networks.iterator();
		this.messagePanel.remove(NetworkSelectionMessage.getInstance());

		updateUI();
		this.messagePanel.updateUI();
		while (it.hasNext()) {
			((Network) it.next()).setBorder(BorderFactory.createEmptyBorder());
		}

		if (((Router) this.selMouseListener.selectedObject).cleanRoutingTable()) {
			JOptionPane.showMessageDialog(this,
					"Router's routing table has been changed.", "Warning", 2);
		}

		it = this.networks.iterator();
		Network net = null;
		while (it.hasNext()) {
			net = (Network) it.next();
			if ((net.defaultGateway != null)
					&& (!net.routers.contains(net.defaultGateway))) {
				net.defaultGateway = null;
				new ChooseDefaultGateway(net, net.defaultGateway);
			}
		}

		this.selMouseListener.selectedObject = null;
		this.selMouseListener.selectionClass = null;
	}

	public void exitHostSelectionMode() {
		removeMouseListener(this.selMouseListener);
		this.selMouseListener.selectedObject = null;
		this.selMouseListener.selectionClass = null;
		addMouseListener(this.mouseListener);
		setCursor(new Cursor(0));
		this.messagePanel.remove(HostSelectionMessage.getInstance(true));

		updateUI();
		this.messagePanel.updateUI();
		if (this.datagram.getSource() != null) {
			this.datagram.getSource().setBorder(
					BorderFactory.createEmptyBorder());
		}
		if (this.datagram.getTarget() != null) {
			this.datagram.getTarget().setBorder(
					BorderFactory.createEmptyBorder());
		}

		if (this.sourceAndTarget) {
			enterSelectionMode(this.datagram, false);
			this.sourceAndTarget = false;
		} else {
			this.datagram = null;
		}
	}

	public void exitRouterSelectionMode() {
		removeMouseListener(this.selMouseListener);
		this.selMouseListener.selectedObject = null;
		this.selMouseListener.selectionClass = null;
		addMouseListener(this.mouseListener);
		setCursor(new Cursor(0));
		this.messagePanel.remove(RouterSelectionMessage.getInstance(this));

		updateUI();
		this.messagePanel.updateUI();

		Iterator it = this.routers.iterator();
		while (it.hasNext())
			((Router) it.next()).setBorder(BorderFactory.createEmptyBorder());
	}

	public void enterSelectionMode(Router router) {
		setCursor(new Cursor(12));
		removeMouseListener(this.mouseListener);
		addMouseListener(this.selMouseListener);
		this.selMouseListener.selectionClass = Network.class;
		this.selMouseListener.selectedObject = router;
		this.messagePanel.add(NetworkSelectionMessage.getInstance());

		updateUI();
		this.messagePanel.updateUI();
		Iterator it = router.networks.iterator();
		while (it.hasNext())
			((JLabel) it.next()).setBorder(BorderFactory
					.createLineBorder(Color.GREEN));
	}

	public void enterSelectionMode(Network net) {
		Iterator it = net.routers.iterator();

		if (net.routers.size() == 1) {
			net.defaultGateway = ((Router) it.next());
			return;
		}
		if (net.routers.size() == 0) {
			JOptionPane
					.showMessageDialog(
							this,
							"First connect routers to network.\nOnly then you will be able to choose a default gateway for the network.");

			return;
		}

		Router r = null;
		while (it.hasNext()) {
			r = (Router) it.next();
			r.setBorder(BorderFactory.createLineBorder(Color.BLUE));
		}

		if (net.defaultGateway != null) {
			net.defaultGateway.setBorder(BorderFactory
					.createLineBorder(Color.GREEN));
		}

		setCursor(new Cursor(12));
		removeMouseListener(this.mouseListener);
		addMouseListener(this.selMouseListener);
		this.selMouseListener.selectionClass = Router.class;
		this.selMouseListener.selectedObject = net;
		this.messagePanel.add(RouterSelectionMessage.getInstance(this));

		updateUI();
		this.messagePanel.updateUI();
	}

	public void enterSelectionMode(Datagram datagram, boolean source) {
		this.datagram = datagram;
		setCursor(new Cursor(12));
		removeMouseListener(this.mouseListener);
		addMouseListener(this.selMouseListener);
		this.selMouseListener.selectionClass = Host.class;
		this.selMouseListener.selectedObject = datagram;
		this.selMouseListener.source = source;
		this.messagePanel.add(HostSelectionMessage.getInstance(source));

		updateUI();
		this.messagePanel.updateUI();

		Host h = (source) ? datagram.getSource() : datagram.getTarget();
		if (h != null)
			h.setBorder(BorderFactory.createLineBorder(Color.GREEN));
	}

	public Network createNewNetwork() {
		Network network = new Network(this);
		network.setHorizontalAlignment(10);
		if (this.networkImage == null) {
			this.networkImage = loadImage("network2.gif");
		}

		while (this.networkImage.getImageLoadStatus() != 8)
			;
		network.setIconTextGap(11 - this.networkImage.getIconWidth());

		network.setIcon(this.networkImage);
		network.setSize(network.getPreferredSize());
		return network;
	}

	public void removeNetwork(Network network) {
		remove(network);
		this.networks.remove(network);
		removeNetworkConnections(network);
	}

	public void removeNetworkConnections(Network network) {
		ListIterator it = network.hosts.listIterator();

		while (it.hasNext()) {
			Host host = (Host) it.next();
			removeFromDatagrams(host);
			remove(host);
		}

		network.hosts.clear();

		it = network.routers.listIterator();
		boolean rtChanged = false;
		while (it.hasNext()) {
			Router router = (Router) it.next();
			if (router.detachFromNetwork(network)) {
				rtChanged = true;
			}
		}

		if (rtChanged)
			JOptionPane.showMessageDialog(this,
					"One or more connected routers' routing tables affected.",
					"Warning", 2);
	}

	public void removeRouter(Router router) {
		remove(router);
		this.routers.remove(router);

		Iterator it1 = this.networks.listIterator();
		while (it1.hasNext()) {
			Network net = (Network) it1.next();
			if (router.equals(net.defaultGateway)) {
				net.defaultGateway = null;
				new ChooseDefaultGateway(net, router);
			}

			if (net.routers.contains(router)) {
				net.routers.remove(router);
				net.deAllocateIP(router);
			}
		}
	}

	public void removeHost(Host host) {
		remove(host);

		Iterator it = this.networks.iterator();
		while (it.hasNext()) {
			Network net = (Network) it.next();
			if (net.hosts.contains(host)) {
				removeFromDatagrams(host);
				net.hosts.remove(host);
				net.deAllocateIP(host);
			}
		}
	}

	public void removeFromDatagrams(Host host) {
		Iterator it = this.datagrams.iterator();
		while (it.hasNext()) {
			Datagram datagram = (Datagram) it.next();
			if (host == datagram.getSource()) {
				datagram.setSource(null);
			}

			if (host == datagram.getTarget())
				datagram.setTarget(null);
		}
	}

	public void removeDatagram(Datagram datagram) {
		remove(datagram);
	}

	public Router createNewRouter() {
		Router router = new Router("");
		router.setHorizontalAlignment(10);
		if (this.routerImage == null) {
			this.routerImage = loadImage("router.gif");
		}

		router.setIconTextGap(5 - this.routerImage.getIconWidth());
		router.setIcon(this.routerImage);
		router.setSize(router.getPreferredSize());
		return router;
	}

	public Host createNewHost(Network net) {
		Host host = new Host("", net);
		host.setHorizontalAlignment(10);
		if (this.hostImage == null) {
			this.hostImage = loadImage("host.gif");
		}

		host.setIconTextGap(5 - this.hostImage.getIconWidth());
		host.setIcon(this.hostImage);
		host.setSize(host.getPreferredSize());
		return host;
	}

	public Datagram createNewDatagram() {
		Datagram dg = new Datagram("");
		dg.setHorizontalAlignment(10);
		if (this.envelopeImage == null) {
			this.envelopeImage = loadImage("envelope.gif");
		}

		dg.main = this.envelopeImage;
		if (this.fragImage == null) {
			this.fragImage = loadImage("frag.gif");
		}
		dg.frag = this.fragImage;
		dg.setIconTextGap(5 - this.envelopeImage.getIconWidth());
		dg.setIcon(this.envelopeImage);
		dg.setSize(dg.getPreferredSize());
		return dg;
	}

	public ImageIcon loadImage(String path) {
		int count = 0;
		BufferedInputStream imgStream = new BufferedInputStream(super
				.getClass().getClassLoader().getResourceAsStream(path));
		if (imgStream != null) {
			try {
				count = imgStream.read(this.buf);
				imgStream.close();
			} catch (IOException ioe) {
				System.err.println("Couldn't read stream from file: " + path);
				return null;
			}
			if (count <= 0) {
				System.err.println("Empty file: " + path);
				return null;
			}
			return new ImageIcon(Toolkit.getDefaultToolkit().createImage(
					this.buf));
		}
		System.err.println("Couldn't find file: " + path);
		return null;
	}
	public boolean isIPv6() {
		return isIPv6;
	}

	public void setIPv6(boolean isIPv6) {
		this.isIPv6 = isIPv6;
	}
}

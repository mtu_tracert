/*    */ package mtutr.windows;
/*    */ 
/*    */ import javax.swing.InputVerifier;
/*    */ import javax.swing.JComponent;
/*    */ import javax.swing.JTextField;
/*    */ 
/*    */ public class IPInputVerifier extends InputVerifier
/*    */ {
/*    */   public boolean verify(JComponent comp)
/*    */   {
/* 10 */     JTextField field = (JTextField)comp;
/* 11 */     String strValue = field.getText();
/*    */     try
/*    */     {
/* 14 */       int value = Integer.parseInt(strValue);
/*    */ 
/* 16 */       return (value >= 0) && (value <= 255);
/*    */     } catch (NumberFormatException e) {
/*    */     }
/* 19 */     return false;
/*    */   }
/*    */ }

/* Location:           C:\Users\MT\Desktop\mtutr.jar
 * Qualified Name:     mtutr.windows.IPInputVerifier
 * JD-Core Version:    0.5.4
 */
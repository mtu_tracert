/*    */ package mtutr.windows;
/*    */ 
/*    */ /*    */ import java.awt.event.ActionEvent;
/*    */ import java.awt.event.ActionListener;
/*    */ import java.util.Iterator;
/*    */ import java.util.List;
import javax.swing.JOptionPane;

import mtutr.common.IPAddress;
import mtutr.objects.Network;
import mtutr.objects.Router;
/*    */ 
/*    */ public class ChooseDefaultGateway
/*    */   implements ActionListener
/*    */ {
/*    */   public ChooseDefaultGateway(Network net, Router r)
/*    */   {
/* 18 */     if (net.routers.size() == 2)
/*    */     {
/* 20 */       if (r.equals(net.routers.get(0)))
/* 21 */         net.defaultGateway = ((Router)net.routers.get(1));
/*    */       else {
/* 23 */         net.defaultGateway = ((Router)net.routers.get(0));
/*    */       }
/*    */     }
/* 26 */     else if (net.routers.size() > 1)
/*    */     {
/* 28 */       String[] box = new String[net.routers.size() - 1];
/*    */ 
/* 30 */       Iterator it = net.routers.iterator();
/* 31 */       int i = 0;
/* 32 */       while (it.hasNext()) {
/* 33 */         Router router = (Router)it.next();
/* 34 */         if (!router.equals(r)) {
/* 35 */           box[i] = router.getIP(net).getIPStr();
/* 36 */           ++i;
/*    */         }
/*    */ 
/*    */       }
/*    */ 
/* 41 */       String st = (String)JOptionPane.showInputDialog(null, 
/* 42 */         "<HTML>The Router you are removing is a default gateway for<BR>network " + 
/* 43 */         net.getIP().getIPStr() + ".<BR><BR>" + 
/* 44 */         "Please choose an alternative default gateway for the network:</HTML>", 
/* 45 */         "Default Gateway Removal", 3, null, box, box[0]);
/*    */ 
/* 47 */       i = 0;
/* 48 */       it = net.routers.iterator();
/* 49 */       while (it.hasNext()) {
/* 50 */         Router router = (Router)it.next();
/* 51 */         if (router.getIP(net).getIPStr().equals(st)) {
/* 52 */           net.defaultGateway = router;
/* 53 */           return;
/*    */         }
/*    */       }
/*    */ 
/*    */     }
/*    */     else
/*    */     {
/* 60 */       JOptionPane.showMessageDialog(null, 
/* 61 */         "<HTML>Attention: You have just removed the default gateway of<BR>network " + 
/* 62 */         net.getIP().getIPStr() + ".<BR><BR>" + 
/* 63 */         "Notice that the network now has no default gateway.</HTML>", 
/* 64 */         "Default Gateway Removal", 1, null);
/*    */     }
/*    */   }
/*    */ 
/*    */   public void actionPerformed(ActionEvent e)
/*    */   {
/*    */   }
/*    */ }

/* Location:           C:\Users\MT\Desktop\mtutr.jar
 * Qualified Name:     mtutr.windows.ChooseDefaultGateway
 * JD-Core Version:    0.5.4
 */
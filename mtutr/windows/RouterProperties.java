/*     */ package mtutr.windows;
/*     */ 
/*     */ /*     */ import java.awt.Color;
/*     */ import java.awt.Container;
/*     */ import java.awt.Dimension;
/*     */ import java.awt.Font;
/*     */ import java.awt.GridLayout;
/*     */ import java.awt.event.ActionEvent;
/*     */ import java.awt.event.ActionListener;
/*     */ import java.awt.event.WindowEvent;
/*     */ import java.awt.event.WindowListener;
/*     */ import java.util.Iterator;
/*     */ import java.util.List;
/*     */ import javax.swing.DefaultCellEditor;
/*     */ import javax.swing.JButton;
/*     */ import javax.swing.JCheckBox;
/*     */ import javax.swing.JComboBox;
/*     */ import javax.swing.JFrame;
/*     */ import javax.swing.JLabel;
/*     */ import javax.swing.JOptionPane;
/*     */ import javax.swing.JPanel;
/*     */ import javax.swing.JScrollPane;
/*     */ import javax.swing.JTable;
/*     */ import javax.swing.table.TableCellEditor;
/*     */ import javax.swing.table.TableColumn;
import javax.swing.table.TableColumnModel;

import mtutr.common.IPAddress;
import mtutr.objects.Network;
import mtutr.objects.Router;
import mtutr.objects.RoutingTable;
import mtutr.windows.buttonhandlers.ButtonEditor;
import mtutr.windows.buttonhandlers.ButtonRenderer;
/*     */ 
/*     */ public class RouterProperties extends JFrame
/*     */   implements ActionListener, WindowListener
/*     */ {
/*     */   private static final long serialVersionUID = 1L;
/*     */   private Router router;
/*     */   private JComboBox intefaces;
/*     */   private JComboBox gateways;
/*     */   private JTable routing_table;
/*     */   private RoutingTable tableModel;
/*  41 */   private JLabel errMsg = null;
/*     */ 
/*     */   public RouterProperties(Router theRouter)
/*     */   {
/*  46 */     init(theRouter);
/*     */   }
/*     */ 
/*     */   private void init(Router theRouter) {
/*  50 */     this.router = theRouter;
/*     */ 
/*  52 */     setTitle("Router Properties");
/*  53 */     Container c = getContentPane();
/*  54 */     c.setLayout(new GridLayout(0, 1));
/*  55 */     setSize(new Dimension(480, 310));
/*  56 */     setResizable(false);
/*     */ 
/*  58 */     JPanel routing_table_panel = new JPanel();
/*  59 */     routing_table_panel.setSize(new Dimension(480, 270));
/*  60 */     c.add(routing_table_panel);
/*     */ 
/*  62 */     this.errMsg = new JLabel("");
/*  63 */     Font font = new Font("Tahoma", 1, 12);
/*  64 */     this.errMsg.setFont(font);
/*  65 */     this.errMsg.setForeground(Color.red);
/*  66 */     this.errMsg.setSize(this.errMsg.getPreferredSize());
/*  67 */     this.errMsg.setLocation(1, 1);
/*  68 */     routing_table_panel.add(this.errMsg);
/*     */ 
/*  70 */     this.tableModel = this.router.routingTable;
/*  71 */     this.tableModel.openedInEditor(this.errMsg);
/*  72 */     this.routing_table = new JExtendedTable(this.tableModel);
/*  73 */     JScrollPane scrollpane = new JScrollPane(this.routing_table);
/*  74 */     scrollpane.setPreferredSize(new Dimension(450, 180));
/*  75 */     scrollpane.setMaximumSize(new Dimension(450, 180));
/*  76 */     routing_table_panel.add(scrollpane);
/*  77 */     TableColumn column = null;
/*  78 */     column = this.routing_table.getColumnModel().getColumn(0);
/*     */ 
/*  80 */     column.setHeaderValue("Network Destination");
/*     */ 
/*  82 */     column = this.routing_table.getColumnModel().getColumn(1);
/*     */ 
/*  84 */     column.setHeaderValue("Netmask");
/*     */ 
/*  88 */     this.gateways = new JComboBox();
/*  89 */     this.intefaces = new JComboBox();
/*     */ 
/*  91 */     Iterator it = this.router.networks.iterator();
/*     */ 
/*  94 */     while (it.hasNext())
/*     */     {
/*  96 */       Network net = (Network)it.next();
/*  97 */       this.intefaces.addItem(net.getIP().getIPStr());
/*  98 */       Iterator rIt = net.routers.iterator();
/*  99 */       while (rIt.hasNext()) {
/* 100 */         Router tmpRouter = (Router)rIt.next();
/* 101 */         if (this.router != tmpRouter) {
/* 102 */           this.gateways.addItem(net.allocateIP(tmpRouter).getIPStr());
/*     */         }
/*     */         else
/*     */         {
/* 106 */           this.gateways.addItem(net.allocateIP(tmpRouter).getIPStr());
/*     */         }
/*     */       }
/*     */     }
/*     */ 
/* 111 */     column = this.routing_table.getColumnModel().getColumn(2);
/* 112 */     column.setHeaderValue("Next Hop");
/* 113 */     column.setCellEditor(new DefaultCellEditor(this.gateways));
/*     */ 
/* 115 */     column = this.routing_table.getColumnModel().getColumn(3);
/* 116 */     column.setHeaderValue("Interface");
/* 117 */     column.setCellEditor(new DefaultCellEditor(this.intefaces));
/*     */ 
/* 119 */     column = this.routing_table.getColumnModel().getColumn(4);
/* 120 */     column.setHeaderValue("Remove line");
/* 121 */     column.setCellEditor(new ButtonEditor(new JCheckBox(), this.routing_table, this.tableModel));
/* 122 */     column.setCellRenderer(new ButtonRenderer());
/*     */ 
/* 124 */     JButton saveBtn = new JButton("Save");
/* 125 */     saveBtn.addActionListener(this);
/* 126 */     routing_table_panel.add(saveBtn);
/*     */ 
/* 128 */     JButton cancelBtn = new JButton("Cancel");
/* 129 */     cancelBtn.addActionListener(this);
/* 130 */     routing_table_panel.add(cancelBtn);
/*     */ 
/* 132 */     JButton clearBtn = new JButton("Clear");
/* 133 */     clearBtn.addActionListener(this);
/* 134 */     routing_table_panel.add(clearBtn);
/*     */ 
/* 136 */     JButton newLineBtn = new JButton("Add new Line");
/* 137 */     newLineBtn.addActionListener(this);
/* 138 */     routing_table_panel.add(newLineBtn);
/*     */ 
/* 140 */     if (this.router.networks.size() != 0) {
/* 141 */       JButton basicConfigBtn = new JButton("Add entries for each interface");
/* 142 */       basicConfigBtn.addActionListener(this);
/* 143 */       routing_table_panel.add(basicConfigBtn);
/*     */ 
/* 145 */       JButton defEntryBtn = new JButton("Add default entry");
/* 146 */       defEntryBtn.addActionListener(this);
/* 147 */       routing_table_panel.add(defEntryBtn);
/*     */     }
/*     */ 
/* 150 */     addWindowListener(this);
/* 151 */     setDefaultCloseOperation(0);
/*     */   }
/*     */ 
/*     */   public void actionPerformed(ActionEvent e)
/*     */   {
/* 156 */     String command = e.getActionCommand();
/* 157 */     if (command.equalsIgnoreCase("Add new Line")) {
/* 158 */       this.tableModel.addRow();
/*     */     }
/* 160 */     else if (command.equalsIgnoreCase("Cancel")) {
/* 161 */       this.tableModel.rollbackAfterChange();
/* 162 */       dispose();
/*     */     }
/* 164 */     else if (command.equalsIgnoreCase("Save"))
/*     */     {
/* 166 */       if (this.routing_table.isEditing()) {
/* 167 */         int row = this.routing_table.getSelectedRow();
/* 168 */         int column = this.routing_table.getSelectedColumn();
/* 169 */         TableCellEditor tce = this.routing_table.getCellEditor(row, column);
/* 170 */         tce.stopCellEditing();
/*     */       }
/*     */ 
/* 173 */       String msg = this.tableModel.validateData();
/* 174 */       if (msg != null) {
/* 175 */         JOptionPane.showMessageDialog(this, msg, "Error", 0);
/*     */       }
/*     */       else {
/* 178 */         this.tableModel.commitData();
/* 179 */         dispose();
/*     */       }
/*     */     }
/* 182 */     else if (command.equalsIgnoreCase("Clear")) {
/* 183 */       this.router.routingTable.clear();
/*     */     }
/* 185 */     else if (command.equalsIgnoreCase("Add entries for each interface")) {
/* 186 */       this.router.routingTable.addEntriesForInterface();
/*     */     }
/* 188 */     else if (command.equalsIgnoreCase("Add default entry")) {
/* 189 */       this.router.routingTable.addDefaultEntry();
/*     */     }
/*     */   }
/*     */ 
/*     */   public void windowOpened(WindowEvent arg0)
/*     */   {
/*     */   }
/*     */ 
/*     */   public void windowClosing(WindowEvent event)
/*     */   {
/* 203 */     if (JOptionPane.showConfirmDialog(this, 
/* 204 */       "All changes are going to be discarted. Are you sure?", 
/* 205 */       "Close window", 
/* 206 */       0) == 0) {
/* 207 */       this.tableModel.rollbackAfterChange();
/* 208 */       dispose();
/*     */     }
/*     */     else {
/* 211 */       event.setSource(null);
/*     */     }
/*     */   }
/*     */ 
/*     */   public void windowClosed(WindowEvent arg0)
/*     */   {
/*     */   }
/*     */ 
/*     */   public void windowIconified(WindowEvent arg0)
/*     */   {
/*     */   }
/*     */ 
/*     */   public void windowDeiconified(WindowEvent arg0)
/*     */   {
/*     */   }
/*     */ 
/*     */   public void windowActivated(WindowEvent arg0)
/*     */   {
/*     */   }
/*     */ 
/*     */   public void windowDeactivated(WindowEvent arg0)
/*     */   {
/*     */   }
/*     */ }

/* Location:           C:\Users\MT\Desktop\mtutr.jar
 * Qualified Name:     mtutr.windows.RouterProperties
 * JD-Core Version:    0.5.4
 */
/*    */ package mtutr.windows;
/*    */ 
/*    */ /*    */ import javax.swing.JTable;
import javax.swing.event.ListSelectionEvent;

import mtutr.objects.RoutingTable;
/*    */ 
/*    */ public class JExtendedTable extends JTable
/*    */ {
/*    */   private static final long serialVersionUID = 1L;
/*    */   private RoutingTable routingTable;
/*    */ 
/*    */   public JExtendedTable(RoutingTable theRoutingTable)
/*    */   {
/* 13 */     super(theRoutingTable);
/* 14 */     this.routingTable = theRoutingTable;
/*    */   }
/*    */ 
/*    */   public void valueChanged(ListSelectionEvent e) {
/* 18 */     super.valueChanged(e);
/* 19 */     if (e.getFirstIndex() == e.getLastIndex())
/* 20 */       return;
/*    */     int trg;
/*    */     int src;
/* 24 */     if (e.getFirstIndex() == getSelectedRow()) {
/* 25 */       src = e.getLastIndex();
/* 26 */       trg = e.getFirstIndex();
/*    */     }
/*    */     else {
/* 29 */       trg = e.getLastIndex();
/* 30 */       src = e.getFirstIndex();
/*    */     }
/*    */ 
/* 33 */     this.routingTable.selectionChanged(src, trg);
/*    */   }
/*    */ }

/* Location:           C:\Users\MT\Desktop\mtutr.jar
 * Qualified Name:     mtutr.windows.JExtendedTable
 * JD-Core Version:    0.5.4
 */
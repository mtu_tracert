/*     */ package mtutr.windows;
/*     */ 
/*     */ /*     */ import java.awt.Container;
/*     */ import java.awt.Dimension;
/*     */ import java.awt.GridLayout;
/*     */ import java.awt.event.ActionEvent;
/*     */ import java.awt.event.ActionListener;
/*     */ import java.util.Iterator;
/*     */ import java.util.List;
/*     */ import javax.swing.ButtonGroup;
/*     */ import javax.swing.JButton;
/*     */ import javax.swing.JFrame;
/*     */ import javax.swing.JLabel;
/*     */ import javax.swing.JOptionPane;
/*     */ import javax.swing.JPanel;
/*     */ import javax.swing.JRadioButton;
/*     */ import javax.swing.JSpinner;
/*     */ import javax.swing.SpinnerNumberModel;
/*     */ import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;

import mtutr.MainDrawBoard;
import mtutr.common.IPAddress;
import mtutr.objects.Network;
/*     */ 
/*     */ public class NetworkProperties extends JFrame
/*     */   implements ActionListener
/*     */ {
/*     */   private static final long serialVersionUID = 1L;
/*  30 */   private JSpinner mask = null;
/*  31 */   private SpinnerNumberModel maskModel = null;
/*  32 */   private JLabel slash = null;
/*  33 */   private JSpinner ip1 = null;
/*  34 */   private JSpinner ip2 = null;
/*  35 */   private JSpinner ip3 = null;
/*  36 */   private JSpinner ip4 = null;
/*  37 */   private ButtonGroup group = null;
/*  38 */   private JRadioButton _CIDR = null;
/*  39 */   private JRadioButton _class = null;
/*  40 */   private JSpinner MTU = null;
/*  41 */   private JLabel bin = null;
/*     */   private Network network;
/*     */ 
/*     */   public NetworkProperties(Network theNetwork)
/*     */   {
/*  46 */     this.network = theNetwork;
/*     */ 
/*  48 */     setTitle("Network Properties");
/*  49 */     Container c = getContentPane();
/*  50 */     c.setLayout(new GridLayout(0, 1));
/*  51 */     setSize(new Dimension(450, 200));
/*  52 */     setResizable(false);
/*     */ 
/*  54 */     JPanel addressing_panel = new JPanel();
/*  55 */     c.add(addressing_panel);
/*     */ 
/*  57 */     JLabel addressing = new JLabel("Addressing Type: ");
/*  58 */     addressing_panel.add(addressing);
/*  59 */     this._class = new JRadioButton("Class Based", true);
/*  60 */     addressing_panel.add(this._class);
/*  61 */     this._class.addActionListener(new ActionListener()
/*     */     {
/*     */       public void actionPerformed(ActionEvent e) {
/*  64 */         if (NetworkProperties.this.mask != null)
/*     */         {
/*  66 */           NetworkProperties.this.mask.setEnabled(false);
/*     */         }
/*  68 */         if (NetworkProperties.this.slash == null)
/*     */           return;
/*  70 */         NetworkProperties.this.slash.setEnabled(false);
/*     */       }
/*     */     });
/*  74 */     this._CIDR = new JRadioButton("CIDR Based");
/*  75 */     addressing_panel.add(this._CIDR);
/*  76 */     this._CIDR.addActionListener(new ActionListener()
/*     */     {
/*     */       public void actionPerformed(ActionEvent e) {
/*  79 */         if (NetworkProperties.this.mask != null)
/*     */         {
/*  81 */           NetworkProperties.this.mask.setEnabled(true);
/*     */         }
/*  83 */         if (NetworkProperties.this.slash == null)
/*     */           return;
/*  85 */         NetworkProperties.this.slash.setEnabled(true);
/*     */       }
/*     */     });
/*  89 */     this.group = new ButtonGroup();
/*  90 */     this.group.add(this._CIDR);
/*  91 */     this.group.add(this._class);
/*     */ 
/*  93 */     JPanel IPPanel = new JPanel();
/*  94 */     c.add(IPPanel);
/*  95 */     IPPanel.add(new JLabel("Network IP Address:"));
/*  96 */     this.ip1 = new JSpinner(new SpinnerNumberModel(1, 1, 255, 1));
/*  97 */     this.ip1.setPreferredSize(new Dimension(40, 20));
/*  98 */     this.ip2 = new JSpinner(new SpinnerNumberModel(0, 0, 255, 1));
/*  99 */     this.ip2.setPreferredSize(new Dimension(40, 20));
/* 100 */     this.ip3 = new JSpinner(new SpinnerNumberModel(0, 0, 255, 1));
/* 101 */     this.ip3.setPreferredSize(new Dimension(40, 20));
/* 102 */     this.ip4 = new JSpinner(new SpinnerNumberModel(0, 0, 255, 1));
/* 103 */     this.ip4.setPreferredSize(new Dimension(40, 20));
/*     */ 
/* 105 */     this.ip1.addChangeListener(new ChangeListener() {
/*     */       public void stateChanged(ChangeEvent e) {
/* 107 */         NetworkProperties.this.updateMaskSpinner();
/*     */       }
/*     */     });
/* 111 */     this.ip2.addChangeListener(new ChangeListener() {
/*     */       public void stateChanged(ChangeEvent e) {
/* 113 */         NetworkProperties.this.updateMaskSpinner();
/*     */       }
/*     */     });
/* 117 */     this.ip3.addChangeListener(new ChangeListener() {
/*     */       public void stateChanged(ChangeEvent e) {
/* 119 */         NetworkProperties.this.updateMaskSpinner();
/*     */       }
/*     */     });
/* 123 */     this.ip4.addChangeListener(new ChangeListener() {
/*     */       public void stateChanged(ChangeEvent e) {
/* 125 */         NetworkProperties.this.updateMaskSpinner();
/*     */       }
/*     */     });
/* 130 */     IPPanel.add(this.ip1);
/* 131 */     IPPanel.add(new JLabel("."));
/* 132 */     IPPanel.add(this.ip2);
/* 133 */     IPPanel.add(new JLabel("."));
/* 134 */     IPPanel.add(this.ip3);
/* 135 */     IPPanel.add(new JLabel("."));
/* 136 */     IPPanel.add(this.ip4);
/* 137 */     this.slash = new JLabel("/");
/* 138 */     this.slash.setEnabled(false);
/* 139 */     this.maskModel = new SpinnerNumberModel(1, 1, 31, 1);
/* 140 */     this.mask = new JSpinner(this.maskModel);
/* 141 */     this.mask.setPreferredSize(new Dimension(40, 20));
/* 142 */     this.mask.setEnabled(false);
/* 143 */     IPPanel.add(this.slash);
/* 144 */     IPPanel.add(this.mask);
/*     */ 
/* 146 */     JPanel binPresentation = new JPanel();
/* 147 */     this.bin = new JLabel("Binary Representation: 00000001.00000000.00000000.00000000");
/* 148 */     binPresentation.add(this.bin);
/* 149 */     c.add(binPresentation);
/*     */ 
/* 151 */     JPanel MTUPanel = new JPanel();
/* 152 */     MTUPanel.add(new JLabel("MTU: "));
/* 153 */     this.MTU = new JSpinner(new SpinnerNumberModel(500, 1, 2147483647, 1));
/* 154 */     this.MTU.setPreferredSize(new Dimension(70, 20));
/* 155 */     MTUPanel.add(this.MTU);
/* 156 */     c.add(MTUPanel);
/*     */ 
/* 158 */     JPanel buttons = new JPanel();
/* 159 */     JButton ok = new JButton("OK");
/* 160 */     ok.addActionListener(this);
/* 161 */     buttons.add(ok);
/* 162 */     JButton cancel = new JButton("Cancel");
/* 163 */     cancel.addActionListener(this);
/* 164 */     buttons.add(cancel);
/*     */ 
/* 166 */     c.add(buttons);
/*     */ 
/* 168 */     if (this.network.isAssigned()) {
/* 169 */       IPAddress netIP = this.network.getIP();
/* 170 */       this.ip1.setValue(netIP.getIPByteInt(3));
/* 171 */       this.ip2.setValue(netIP.getIPByteInt(2));
/* 172 */       this.ip3.setValue(netIP.getIPByteInt(1));
/* 173 */       this.ip4.setValue(netIP.getIPByteInt(0));
/*     */ 
/* 175 */       this.MTU.setValue(new Integer(this.network.getMTU()));
/* 176 */       if (this.network.getIsCIDR()) {
/* 177 */         this._CIDR.setSelected(true);
/* 178 */         this.mask.setValue(new Integer(this.network.getCIDR()));
/* 179 */         this.mask.setEnabled(true);
/*     */       }
/*     */     }
/*     */   }
/*     */ 
/*     */   public void updateMaskSpinner() {
/* 185 */     IPAddress ip = new IPAddress(0L);
/* 186 */     ip.setIP(
/* 187 */       (Integer)this.ip1.getValue(), 
/* 188 */       (Integer)this.ip2.getValue(), 
/* 189 */       (Integer)this.ip3.getValue(), 
/* 190 */       (Integer)this.ip4.getValue());
/*     */ 
/* 192 */     int minimalMask = ip.getMinimalMask();
/* 193 */     if (minimalMask == 32) {
/* 194 */       minimalMask = 31;
/*     */     }
/*     */ 
/* 197 */     this.maskModel.setMinimum(new Integer(minimalMask));
/* 198 */     if (((Integer)this.maskModel.getValue()).intValue() < minimalMask) {
/* 199 */       this.maskModel.setValue(new Integer(minimalMask));
/*     */     }
/*     */ 
/* 202 */     this.bin.setText("Binary Representation: " + ip.getBinStrIP());
/*     */   }
/*     */ 
/*     */   public void actionPerformed(ActionEvent e)
/*     */   {
/* 207 */     if (e.getActionCommand().equals("OK"))
/*     */     {
/* 209 */       okPressed();
/*     */     }
/*     */     else
/*     */     {
/* 213 */       cancelPressed();
/*     */     }
/*     */   }
/*     */ 
/*     */   public void okPressed()
/*     */   {
/* 219 */     IPAddress ip = new IPAddress(0L);
/* 220 */     ip.setIP(
/* 221 */       (Integer)this.ip1.getValue(), 
/* 222 */       (Integer)this.ip2.getValue(), 
/* 223 */       (Integer)this.ip3.getValue(), 
/* 224 */       (Integer)this.ip4.getValue());
/*     */ 
/* 228 */     boolean invalidAddress = false;
/* 229 */     String msg = null;
/*     */ 
/* 231 */     if (this._CIDR.isSelected()) {
/* 232 */       int cidr = ((Integer)this.mask.getValue()).intValue();
/*     */ 
/* 234 */       if (ip.getNetworkIPByCIDR(cidr).getIPLong() == 0L) {
/* 235 */         invalidAddress = true;
/* 236 */         msg = "Network address consist of all zeros";
/*     */       }
/* 238 */       else if (ip.getNetworkIPByCIDR(cidr).getIPLong() != ip.getIPLong()) {
/* 239 */         invalidAddress = true;
/* 240 */         msg = "Network IP does not fit to CIDR \n(CIDR mask does not cover network IP)";
/*     */       }
/*     */ 
/*     */     }
/* 245 */     else if (((Integer)this.ip1.getValue()).intValue() >= 224) {
/* 246 */       invalidAddress = true;
/* 247 */       msg = "Class D is not supported";
/*     */     }
/* 249 */     else if (ip.getNetworkIPByCIDR(ip.getIPClass()).getIPLong() != ip.getIPLong()) {
/* 250 */       invalidAddress = true;
/* 251 */       String className = Network.getClassName(ip.getIPClass());
/* 252 */       msg = "Network address does not fit class " + className + "\n" + 
/* 253 */         "Maximum number of bits alloscated for network at class " + className + " is " + 
/* 254 */         ip.getIPClass() + " bits";
/*     */     }
/*     */ 
/* 257 */     if (invalidAddress) {
/* 258 */       JOptionPane.showMessageDialog(this, "Address is not valid: " + msg, "Error", 0);
/* 259 */       return;
/*     */     }
/*     */ 
/* 263 */     Iterator it = MainDrawBoard.staticInstance.networks.iterator();
/* 264 */     Network compNet = null;
/* 265 */     while (it.hasNext()) {
/* 266 */       compNet = (Network)it.next();
/* 267 */       if (compNet == this.network) continue; if (!compNet.isAssigned()) {
/*     */         continue;
/*     */       }
/*     */ 
/* 271 */       if (!ip.equals(compNet.getIP()))
/*     */         continue;
/*     */       int c1;
/* 273 */       if (this._CIDR.isSelected()) {
/* 274 */         c1 = ((Integer)this.mask.getValue()).intValue();
/*     */       }
/*     */       else
/* 277 */         c1 = ip.getIPClass();
/*     */       int c2;
/* 280 */       if (compNet.getIsCIDR()) {
/* 281 */         c2 = compNet.getCIDR();
/*     */       }
/*     */       else {
/* 284 */         c2 = compNet.getIP().getIPClass();
/*     */       }
/*     */ 
/* 287 */       if (c1 == c2) {
/* 288 */         JOptionPane.showMessageDialog(this, "Address is not valid: network with same address already exists", "Error", 0);
/* 289 */         return;
/*     */       }
/*     */ 
/*     */     }
/*     */ 
/* 296 */     if (this.network.isAssigned())
/*     */     {
/* 298 */       int maxHosts = 0;
/*     */ 
/* 300 */       if (this._CIDR.isSelected()) {
/* 301 */         maxHosts = (0x2 ^ 32 - ((Integer)this.mask.getValue()).intValue()) - 1;
/*     */       }
/*     */       else {
/* 304 */         switch (ip.getIPClass())
/*     */         {
/*     */         case 8:
/* 306 */           maxHosts = 16777214;
/* 307 */           break;
/*     */         case 16:
/* 309 */           maxHosts = 65534;
/* 310 */           break;
/*     */         case 24:
/* 312 */           maxHosts = 254;
/* 313 */           break;
/*     */         case 32:
/* 316 */           maxHosts = 0;
/*     */         }
/*     */ 
/*     */       }
/*     */ 
/* 323 */       if (maxHosts < this.network.hosts.size() + this.network.routers.size()) {
/* 324 */         String[] options = { "Use previous configuration", 
/* 325 */           "Use new configuration", 
/* 326 */           "Edit current configuration" };
/*     */ 
/* 328 */         int selected = JOptionPane.showOptionDialog(null, 
/* 329 */           "The new configuration of the network doesn't allow connecting\nall the hosts \\ routers that were already allocated.\n\nWarning: Using the new configuration will result in deleting all the\nnetwork's hosts and disconnecting from all its routers!\n\nWhat would you like to do?", 
/* 334 */           "Properties Update Confirmation", 
/* 335 */           1, 
/* 336 */           3, null, options, null);
/*     */ 
/* 338 */         if (selected == 0) {
/* 339 */           dispose();
/* 340 */           return;
/* 341 */         }if (selected == 1)
/* 342 */           this.network.removeAllConnections();
/*     */         else {
/* 344 */           return;
/*     */         }
/*     */       }
/*     */ 
/*     */     }
/*     */ 
/* 350 */     if (this._CIDR.isSelected()) {
/* 351 */       this.network.setCIDR_IP(ip, ((Integer)this.mask.getValue()).intValue());
/*     */     }
/*     */     else {
/* 354 */       this.network.setCLASS_IP(ip);
/*     */     }
/*     */ 
/* 357 */     this.network.setMTU((Integer)this.MTU.getValue());
/* 358 */     dispose();
/*     */   }
/*     */ 
/*     */   public void cancelPressed()
/*     */   {
/* 363 */     dispose();
/*     */   }
/*     */ }

/* Location:           C:\Users\MT\Desktop\mtutr.jar
 * Qualified Name:     mtutr.windows.NetworkProperties
 * JD-Core Version:    0.5.4
 */
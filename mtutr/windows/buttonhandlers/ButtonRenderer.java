/*    */ package mtutr.windows.buttonhandlers;
/*    */ 
/*    */ import java.awt.Component;
/*    */ import javax.swing.JButton;
/*    */ import javax.swing.JTable;
/*    */ import javax.swing.UIManager;
/*    */ import javax.swing.table.TableCellRenderer;
/*    */ 
/*    */ public class ButtonRenderer extends JButton
/*    */   implements TableCellRenderer
/*    */ {
/*    */   private static final long serialVersionUID = 1L;
/*    */ 
/*    */   public ButtonRenderer()
/*    */   {
/* 14 */     setOpaque(true);
/*    */   }
/*    */ 
/*    */   public Component getTableCellRendererComponent(JTable table, Object value, boolean isSelected, boolean hasFocus, int row, int column)
/*    */   {
/* 19 */     if (isSelected) {
/* 20 */       setForeground(table.getSelectionForeground());
/* 21 */       setBackground(table.getSelectionBackground());
/*    */     } else {
/* 23 */       setForeground(table.getForeground());
/* 24 */       setBackground(UIManager.getColor("Button.background"));
/*    */     }
/* 26 */     setText((value == null) ? "" : value.toString());
/* 27 */     return this;
/*    */   }
/*    */ }

/* Location:           C:\Users\MT\Desktop\mtutr.jar
 * Qualified Name:     mtutr.windows.buttonhandlers.ButtonRenderer
 * JD-Core Version:    0.5.4
 */
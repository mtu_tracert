/*    */ package mtutr.windows.buttonhandlers;
/*    */ 
/*    */ /*    */ import java.awt.Component;
/*    */ import java.awt.event.ActionEvent;
/*    */ import java.awt.event.ActionListener;
/*    */ import javax.swing.DefaultCellEditor;
/*    */ import javax.swing.JButton;
/*    */ import javax.swing.JCheckBox;
import javax.swing.JTable;

import mtutr.objects.RoutingTable;
/*    */ 
/*    */ public class ButtonEditor extends DefaultCellEditor
/*    */ {
/*    */   private static final long serialVersionUID = 1L;
/*    */   protected JButton button;
/*    */   private String label;
/*    */   private boolean isPushed;
/*    */   private JTable uiTable;
/*    */   private RoutingTable routingTable;
/*    */ 
/*    */   public ButtonEditor(JCheckBox checkBox, JTable jt, RoutingTable rt)
/*    */   {
/* 22 */     super(checkBox);
/* 23 */     this.uiTable = jt;
/* 24 */     this.routingTable = rt;
/* 25 */     this.button = new JButton();
/* 26 */     this.button.setOpaque(true);
/* 27 */     this.button.addActionListener(new ActionListener() {
/*    */       public void actionPerformed(ActionEvent e) {
/* 29 */         ButtonEditor.this.fireEditingStopped();
/*    */       }
/*    */     });
/*    */   }
/*    */ 
/*    */   public Component getTableCellEditorComponent(JTable table, Object value, boolean isSelected, int row, int column)
/*    */   {
/* 36 */     if (isSelected) {
/* 37 */       this.button.setForeground(table.getSelectionForeground());
/* 38 */       this.button.setBackground(table.getSelectionBackground());
/*    */     } else {
/* 40 */       this.button.setForeground(table.getForeground());
/* 41 */       this.button.setBackground(table.getBackground());
/*    */     }
/* 43 */     this.label = ((value == null) ? "" : value.toString());
/* 44 */     this.button.setText(this.label);
/* 45 */     this.isPushed = true;
/* 46 */     return this.button;
/*    */   }
/*    */ 
/*    */   public Object getCellEditorValue() {
/* 50 */     if (this.isPushed) {
/* 51 */       int selected = this.uiTable.getSelectedRow();
/* 52 */       this.routingTable.deleteRow(this.uiTable.getSelectedRow());
/* 53 */       this.routingTable.fireTableRowsDeleted(selected, selected);
/*    */     }
/* 55 */     this.isPushed = false;
/* 56 */     return new String(this.label);
/*    */   }
/*    */ 
/*    */   public boolean stopCellEditing() {
/* 60 */     this.isPushed = false;
/* 61 */     return super.stopCellEditing();
/*    */   }
/*    */ 
/*    */   protected void fireEditingStopped() {
/* 65 */     super.fireEditingStopped();
/*    */   }
/*    */ }

/* Location:           C:\Users\MT\Desktop\mtutr.jar
 * Qualified Name:     mtutr.windows.buttonhandlers.ButtonEditor
 * JD-Core Version:    0.5.4
 */
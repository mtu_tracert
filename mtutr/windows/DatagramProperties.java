/*    */ package mtutr.windows;
/*    */ 
/*    */ /*    */ import java.awt.Container;
/*    */ import java.awt.Dimension;
/*    */ import java.awt.GridLayout;
/*    */ import java.awt.event.ActionEvent;
/*    */ import java.awt.event.ActionListener;
/*    */ import javax.swing.JButton;
/*    */ import javax.swing.JFrame;
/*    */ import javax.swing.JLabel;
/*    */ import javax.swing.JPanel;
/*    */ import javax.swing.JSpinner;
import javax.swing.SpinnerNumberModel;

import mtutr.common.IPAddress;
import mtutr.objects.Datagram;
import mtutr.objects.Host;
/*    */ 
/*    */ public class DatagramProperties extends JFrame
/*    */   implements ActionListener
/*    */ {
/*    */   private static final long serialVersionUID = 1L;
/*    */   private Datagram datagram;
/*    */   private JSpinner ttl;
/*    */   private JSpinner length;
/*    */ 
/*    */   public DatagramProperties(Datagram thedatagram)
/*    */   {
/* 29 */     this.datagram = thedatagram;
/*    */ 
/* 31 */     setTitle("Datagram Properties");
/* 32 */     Container c = getContentPane();
/* 33 */     c.setLayout(new GridLayout(0, 1));
/* 34 */     setSize(new Dimension(250, 200));
/* 35 */     setResizable(false);
/*    */ 
/* 37 */     JPanel sourcePanel = new JPanel();
/* 38 */     c.add(sourcePanel);
/* 39 */     String s = (this.datagram.getSource() == null) ? "Not Assigned" : this.datagram.getSource().getIP().getIPStr();
/* 40 */     sourcePanel.add(new JLabel("Source Host:   " + s));
/*    */ 
/* 42 */     JPanel targetPanel = new JPanel();
/* 43 */     c.add(targetPanel);
/* 44 */     s = (this.datagram.getTarget() == null) ? "Not Assigned" : this.datagram.getTarget().getIP().getIPStr();
/* 45 */     targetPanel.add(new JLabel("Target Host:   " + s));
/*    */ 
/* 47 */     JPanel lengthPanel = new JPanel();
/* 48 */     c.add(lengthPanel);
/* 49 */     lengthPanel.add(new JLabel("Length: "));
/* 50 */     this.length = new JSpinner(new SpinnerNumberModel(this.datagram.getLength(), 1, 2147483647, 1));
/* 51 */     this.length.setPreferredSize(new Dimension(80, 20));
/* 52 */     lengthPanel.add(this.length);
/*    */ 
/* 54 */     JPanel ttlPanel = new JPanel();
/* 55 */     c.add(ttlPanel);
/* 56 */     ttlPanel.add(new JLabel("TTL: "));
/* 57 */     this.ttl = new JSpinner(new SpinnerNumberModel(this.datagram.getTTL(), 1, 2147483647, 1));
/* 58 */     this.ttl.setPreferredSize(new Dimension(50, 20));
/* 59 */     ttlPanel.add(this.ttl);
/*    */ 
/* 61 */     JPanel buttons = new JPanel();
/* 62 */     JButton ok = new JButton("OK");
/* 63 */     ok.addActionListener(this);
/* 64 */     buttons.add(ok);
/* 65 */     JButton cancel = new JButton("Cancel");
/* 66 */     cancel.addActionListener(this);
/* 67 */     buttons.add(cancel);
/*    */ 
/* 69 */     c.add(buttons);
/* 70 */     show();
/*    */   }
/*    */ 
/*    */   public void actionPerformed(ActionEvent e)
/*    */   {
/* 75 */     if (e.getActionCommand().equals("OK"))
/*    */     {
/* 77 */       okPressed();
/*    */     }
/*    */     else
/*    */     {
/* 81 */       cancelPressed();
/*    */     }
/*    */   }
/*    */ 
/*    */   public void okPressed()
/*    */   {
/* 87 */     this.datagram.setTTL(((Integer)this.ttl.getValue()).intValue());
/* 88 */     this.datagram.setLength(((Integer)this.length.getValue()).intValue());
/* 89 */     dispose();
/*    */   }
/*    */ 
/*    */   public void cancelPressed()
/*    */   {
/* 94 */     dispose();
/*    */   }
/*    */ }

/* Location:           C:\Users\MT\Desktop\mtutr.jar
 * Qualified Name:     mtutr.windows.DatagramProperties
 * JD-Core Version:    0.5.4
 */
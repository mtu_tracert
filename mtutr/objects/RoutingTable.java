package mtutr.objects;

import java.util.Collections;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.ListIterator;

import javax.swing.JLabel;
import javax.swing.table.AbstractTableModel;

import mtutr.common.IPAddress;

public class RoutingTable extends AbstractTableModel {
	private static final long serialVersionUID = 1L;
	public LinkedList entries = new LinkedList();
	public LinkedList backupDeletedEntries = null;
	boolean val;
	Router router;
	JLabel errMsgContainer;

	public void addDefaultEntry() {
		RoutingTableEntry rte = new RoutingTableEntry();
		rte.net = "0.0.0.0";
		rte.cidr = "0";
		this.entries.add(rte);
		fireTableRowsInserted(1, 1);
	}

	public void addEntriesForInterface() {
		Iterator it = this.router.networks.iterator();
		Network net = null;
		RoutingTableEntry rte = null;
		while (it.hasNext()) {
			net = (Network) it.next();
			rte = new RoutingTableEntry();
			rte.net = net.getIP().toString();
			if (net.getIsCIDR()) {
				rte.cidr = Integer.toString(net.getCIDR());
			} else {
				rte.cidr = Integer.toString(net.getIP().getIPClass());
			}
			rte.gateway = this.router.getIP(net).toString();
			rte.ifc = net.getIP().toString();
			this.entries.add(rte);
		}

		if (this.router.networks.size() != 0)
			fireTableRowsInserted(1, this.router.networks.size());
	}

	public void clear() {
		if (this.backupDeletedEntries == null) {
			this.backupDeletedEntries = this.entries;
		}
		this.entries = new LinkedList();
		fireTableRowsDeleted(0, 0);
	}

	public void openedInEditor(JLabel comp) {
		this.errMsgContainer = comp;
		backupBeforeEdit();
	}

	public void backupBeforeEdit() {
		Iterator it = this.entries.iterator();
		RoutingTableEntry rte = null;
		while (it.hasNext()) {
			rte = (RoutingTableEntry) it.next();
			rte.backup();
		}
	}

	public void rollbackAfterChange() {
		if (this.backupDeletedEntries != null) {
			this.entries = this.backupDeletedEntries;
			this.backupDeletedEntries = null;
		}
		ListIterator it = this.entries.listIterator();
		RoutingTableEntry rte = null;
		while (it.hasNext()) {
			rte = (RoutingTableEntry) it.next();
			if (rte.isNew) {
				it.remove();
			} else
				rte.rollback();
		}
	}

	public void commitData() {
		this.backupDeletedEntries = null;
		Iterator it = this.entries.iterator();
		while (it.hasNext())
			((RoutingTableEntry) it.next()).isNew = false;
	}

	public RoutingTable(Router r) {
		this.router = r;
	}

	public int getColumnCount() {
		return 5;
	}

	public int getRowCount() {
		return this.entries.size();
	}

	public Object getValueAt(int row, int col) {
		switch (col) {
		case 0:
			return ((RoutingTableEntry) this.entries.get(row)).net;
		case 1:
			return ((RoutingTableEntry) this.entries.get(row)).cidr;
		case 2:
			return ((RoutingTableEntry) this.entries.get(row)).gateway;
		case 3:
			return ((RoutingTableEntry) this.entries.get(row)).ifc;
		case 4:
			return "Remove";
		}
		return null;
	}

	@Override
	public Class getColumnClass(int c) {
		return getValueAt(0, c).getClass();
	}

	@Override
	public boolean isCellEditable(int row, int col) {
		return true;
	}

	@Override
	public void setValueAt(Object value, int row, int col) {
		if (value == null) {
			return;
		}

		switch (col) {
		case 0:
			((RoutingTableEntry) this.entries.get(row)).net = ((String) value);
			break;
		case 1:
			((RoutingTableEntry) this.entries.get(row)).cidr = ((String) value);
			break;
		case 2:
			((RoutingTableEntry) this.entries.get(row)).gateway = ((String) value);
			break;
		case 3:
			((RoutingTableEntry) this.entries.get(row)).ifc = ((String) value);
		case 4:
		}
	}

	public void addRow() {
		this.entries.add(new RoutingTableEntry());
		fireTableRowsInserted(1, 1);
	}

	public void selectionChanged(int from, int to) {
		if (this.entries.size() == 0) {
			return;
		}

		RoutingTableEntry entry = (RoutingTableEntry) this.entries.get(from);
		boolean cidrError = false;
		try {
			Integer.parseInt(entry.cidr);
		} catch (Throwable e) {
			if (!IPAddress.isValidAddress(entry.cidr)) {
				cidrError = true;
			}
		}

		if ((entry.net.equals("")) || (!IPAddress.isValidAddress(entry.net))) {
			this.errMsgContainer.setText("Invalid network entry in row "
					+ (from + 1));
		} else if ((entry.cidr.equals("")) || (cidrError)) {
			this.errMsgContainer.setText("Invalid netmask entry in row "
					+ (from + 1));
		} else if ((entry.ifc.equals(""))
				|| (!IPAddress.isValidAddress(entry.ifc))) {
			this.errMsgContainer.setText("Invalid interface entry in row "
					+ (from + 1));
		} else if ((entry.gateway.equals(""))
				|| (!IPAddress.isValidAddress(entry.gateway)))
			this.errMsgContainer.setText("Invalid gateway entry in row "
					+ (from + 1));
	}

	public String validateData() {
		StringBuffer msg = new StringBuffer();
		int pos = 0;
		Iterator it = this.entries.iterator();
		while (it.hasNext()) {
			if (!((RoutingTableEntry) it.next()).isValid()) {
				msg.append("Invalid syntax in line ");
				msg.append(pos + 1);
				msg.append("\n");
			}
			++pos;
		}

		return (msg.length() == 0) ? null : msg.toString();
	}

	public RouteTestResult findMatch(IPAddress ip) {
		List results = new LinkedList();
		Iterator it = this.entries.iterator();
		RoutingTableEntry rte = null;
		RouteTestResult rtr = null;
		while (it.hasNext()) {
			rte = (RoutingTableEntry) it.next();
			rtr = rte.findMatch(ip);
			if (rtr != null) {
				results.add(rtr);
			}
		}

		if (results.size() == 0) {
			return null;
		}

		Collections.sort(results);
		rtr = (RouteTestResult) results.get(results.size() - 1);
		rtr.network = this.router.getNetwork(rtr.ifc);
		if (rtr.network == null) {
			return null;
		}
		return rtr;
	}

	public void deleteRow(int row) {
		if (this.entries.size() > row) {
			if (this.backupDeletedEntries == null) {
				this.backupDeletedEntries = new LinkedList(this.entries);
			}
			this.entries.remove(row);
		}
	}
}

/*
 * Location: C:\Users\MT\Desktop\mtutr.jar Qualified Name:
 * mtutr.objects.RoutingTable JD-Core Version: 0.5.4
 */
package mtutr.objects;

import java.awt.Component;
import java.awt.Font;
import java.util.Hashtable;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import javax.swing.JLabel;

import mtutr.MainDrawBoard;
import mtutr.common.IPAddress;

public class Network extends JLabel implements IPKitItem {
	private static final long serialVersionUID = 1L;
	public List hosts = new LinkedList();
	public List routers = new LinkedList();
	private IPAddress netAddress;
	private int cidr = 0;
	private int mtu = 0;
	private boolean isCIDR = false;
	private boolean isAssigned = false;
	public Router defaultGateway = null;
	private final MainDrawBoard board;
	private final Map dhcp = new Hashtable();
	long nextHostIP = -1L;

	private IPAddress findNextValidHostIP() {
		long current = this.nextHostIP++;
		IPAddress ipAddr = new IPAddress(0L);
		while (this.nextHostIP != current) {
			if ((this.nextHostIP == -2147483648L)
					|| ((this.isCIDR) && (!this.netAddress.isLegalHostWithCIDR(
							this.nextHostIP, this.cidr)))
					|| ((!this.isCIDR) && (!this.netAddress
							.isLegalHostWithClasses(this.nextHostIP)))) {
				this.nextHostIP = (this.netAddress.getIPLong() + 1L);
			} else {
				ipAddr.setIP(this.nextHostIP);
				if (this.dhcp.containsValue(ipAddr)) {
					this.nextHostIP += 1L;
				} else
					return ipAddr;
			}
		}
		return null;
	}

	public IPAddress allocateIP(Object obj) {
		if (!this.dhcp.containsKey(obj)) {
			IPAddress ip = findNextValidHostIP();
			if (ip == null) {
				return null;
			}

			this.dhcp.put(obj, ip);
			return ip;
		}

		return (IPAddress) this.dhcp.get(obj);
	}

	public void deAllocateIP(Object obj) {
		if (this.dhcp.containsKey(obj))
			this.nextHostIP = (((IPAddress) this.dhcp.remove(obj)).getIPLong() - 1L);
	}

	public Router getRouter(IPAddress ip) {
		Iterator it = this.routers.iterator();
		Router r = null;
		while (it.hasNext()) {
			r = (Router) it.next();
			if (this.dhcp.get(r).equals(ip)) {
				return r;
			}
		}

		return null;
	}

	public Network(MainDrawBoard theBoard) {
		super("<HTML>&nbsp;&nbsp;&nbsp;&nbsp;Not assigned</HTML>");
		setFont(new Font("Tahoma", 1, 11));
		this.board = theBoard;
	}

	public void setCIDR_IP(IPAddress newIP, int _cidr) {
		if ((!this.isAssigned) || (this.netAddress == null) || (!this.isCIDR)
				|| (this.cidr != _cidr) || (!newIP.equals(this.netAddress))) {
			this.netAddress = newIP;
			this.isAssigned = true;
			setText("<HTML><br>"
					+ newIP.getIPStr()
					+ "<br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;/"
					+ _cidr + "</HTML>");

			this.isCIDR = true;
			this.cidr = _cidr;
			this.dhcp.clear();
			this.nextHostIP = newIP.getIPLong();
		}

		MainDrawBoard.staticInstance.repaint();
	}

	public static String getClassName(int className) {
		switch (className) {
		case 8:
			return "A";
		case 16:
			return "B";
		case 24:
			return "C";
		case 32:
			return "D";
		}
		return "undefined";
	}

	public void setCLASS_IP(IPAddress newIP) {
		if ((!this.isAssigned) || (this.netAddress == null) || (this.isCIDR)
				|| (newIP.getIPClass() == this.netAddress.getIPClass())
				|| (!newIP.equals(this.netAddress))) {
			this.netAddress = newIP;
			this.isAssigned = true;
			setText("<HTML><br>"
					+ newIP.getIPStr()
					+ "<br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;("
					+ getClassName(this.netAddress.getIPClass())
					+ ")</body></HTML>");
			this.isCIDR = false;
			this.dhcp.clear();
			this.nextHostIP = newIP.getIPLong();
		}
		MainDrawBoard.staticInstance.repaint();
	}

	public IPAddress getIP() {
		return this.netAddress;
	}

	public int getCIDR() {
		return this.cidr;
	}

	public void setCIDR(Integer theCIDR) {
		this.cidr = theCIDR.intValue();
	}

	public void setCIDR(int theCIDR) {
		this.cidr = theCIDR;
	}

	public void dispatchEvent() {
	}

	public void setMTU(Integer theMTU) {
		this.mtu = theMTU.intValue();
	}

	public void setMTU(int theMTU) {
		this.mtu = theMTU;
	}

	public int getMTU() {
		return this.mtu;
	}

	public boolean getIsCIDR() {
		return this.isCIDR;
	}

	public boolean isAssigned() {
		return this.isAssigned;
	}

	public Component getjComponent() {
		return this;
	}

	public void removeAllConnections() {
		this.board.removeNetworkConnections(this);
	}
}

/*
 * Location: C:\Users\MT\Desktop\mtutr.jar Qualified Name: mtutr.objects.Network
 * JD-Core Version: 0.5.4
 */
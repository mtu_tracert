package mtutr.objects;

import java.awt.Component;

public abstract interface IPKitItem
{
  public abstract void dispatchEvent();

  public abstract Component getjComponent();
}

/* Location:           C:\Users\MT\Desktop\mtutr.jar
 * Qualified Name:     mtutr.objects.IPKitItem
 * JD-Core Version:    0.5.4
 */
package mtutr.objects;

import java.awt.Color;
import java.awt.Component;
import java.awt.Font;
import java.awt.Point;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedList;

import javax.swing.ImageIcon;
import javax.swing.JLabel;
import javax.swing.JOptionPane;

import mtutr.MainDrawBoard;
import mtutr.messages.AnimationMonitor;

public class Datagram extends JLabel implements IPKitItem, Runnable {
	private static final long serialVersionUID = 1L;
	private int ttl;
	private int length;
	private Host source;
	private Host target;
	private final LinkedList path = new LinkedList();
	public ImageIcon main = null;
	public ImageIcon frag = null;

	private int largets_fragment = 0;
	public HashMap fragments = null;

	public Datagram(String string) {
		super(string);
		this.ttl = 10;
		this.length = 500;
	}

	public void dispatchEvent() {
	}

	public Component getjComponent() {
		return this;
	}

	public void setTTL(int i) {
		this.ttl = i;
	}

	public int getTTL() {
		return this.ttl;
	}

	public void setLength(int i) {
		this.length = i;
	}

	public int getLength() {
		return this.length;
	}

	public Host getSource() {
		return this.source;
	}

	public void setSource(Host source) {
		this.source = source;
	}

	public Host getTarget() {
		return this.target;
	}

	public void setTarget(Host target) {
		this.target = target;
	}

	public void send() {
		this.path.clear();
		this.path.add(this.source);
		this.path.add(this.source.network);
		if (this.source.network.equals(this.target.network)) {
			this.path.add(this.target);
		} else if (this.source.network.defaultGateway != null) {
			calculateRoute();
		}

		new Thread(this).start();
	}

	protected void calculateRoute() {
		int routers_count = 0;
		Router router = this.source.network.defaultGateway;
		this.path.add(router);
		RouteTestResult rtr = null;
		Network net = null;
		while (true) {
			rtr = router.routingTable.findMatch(this.target.getIP());
			if (rtr == null) {
				break;
			}

			if (rtr.nextHop.equals(router.getIP(rtr.network))) {
				this.path.add(rtr.network);
				if (!rtr.network.hosts.contains(this.target))
					break;
				this.path.add(this.target);

				break;
			}

			net = rtr.network;
			router = net.getRouter(rtr.nextHop);
			this.path.add(net);

			if (router == null) {
				break;
			}
			this.path.add(router);
			++routers_count;
			if (routers_count > this.ttl + 1) {
				break;
			}

		}
	}

	public void runAnimation() {
		Point origLoc = getLocation();
		int localTTL = this.ttl;

		this.fragments = new HashMap();
		this.fragments.put(new Integer(this.length), new Integer(1));
		this.largets_fragment = this.length;

		MainDrawBoard board = MainDrawBoard.staticInstance;
		AnimationMonitor animMonitor = AnimationMonitor.getInstance();
		animMonitor.updateTTL(localTTL);
		animMonitor.updateMTU(this.length);
		animMonitor.setError("");
		animMonitor.setLegend(null, this);
		animMonitor.enableStop();
		animMonitor.repaint();
		board.removeMouseListener(board.mouseListener);
		board.messagePanel.add(animMonitor);

		board.updateUI();
		board.messagePanel.updateUI();

		Component src = null;
		Component trg = null;
		boolean err = false;

		if (this.path.size() < 3) {
			animMonitor
					.setError("Datagram can not be sent: no default router available.");
		} else {
			for (int i = 1; i < this.path.size(); ++i) {
				if (animMonitor.shouldStop) {
					break;
				}
				src = (Component) this.path.get(i - 1);
				trg = (Component) this.path.get(i);

				if (src instanceof Router) {
					if (localTTL == 0) {
						animMonitor.setError("Datagram expired");
						err = true;
						break;
					}

					--localTTL;
					animMonitor.updateTTL(localTTL);
				}

				if (trg instanceof Network) {
					int mtu = ((Network) trg).getMTU();
					if (mtu < this.largets_fragment) {
						setIcon(this.frag);

						this.largets_fragment = mtu;

						HashMap newFragments = new HashMap();

						Iterator it = this.fragments.keySet().iterator();
						Integer size = null;
						Integer count = null;

						Integer tmpVal = null;
						while (it.hasNext()) {
							size = (Integer) it.next();
							count = (Integer) this.fragments.get(size);
							if (size.intValue() > mtu) {
								int s1 = size.intValue() % mtu;
								int s2 = mtu;

								int c1 = (size.intValue() % mtu == 0) ? 0
										: count.intValue();
								int c2 = size.intValue() / mtu
										* count.intValue();

								if ((s1 != 0) && (c1 != 0)) {
									tmpVal = (Integer) newFragments
											.get(new Integer(s1));
									if (tmpVal == null) {
										newFragments.put(new Integer(s1),
												new Integer(c1));
									} else {
										newFragments.put(new Integer(s1),
												new Integer(c1
														+ tmpVal.intValue()));
									}
								}

								tmpVal = (Integer) newFragments
										.get(new Integer(s2));
								if (tmpVal == null) {
									newFragments.put(new Integer(s2),
											new Integer(c2));
								} else
									newFragments
											.put(new Integer(s2), new Integer(
													c2 + tmpVal.intValue()));
							} else {
								tmpVal = (Integer) newFragments.get(size);
								if (tmpVal == null) {
									newFragments.put(size, count);
								} else {
									newFragments.put(size, new Integer(count
											.intValue()
											+ tmpVal.intValue()));
								}
							}
						}
						this.fragments = newFragments;
						it = this.fragments.values().iterator();
						int acc = 0;
						while (it.hasNext()) {
							acc += ((Integer) it.next()).intValue();
						}

						setFont(new Font("Tahoma", 1, 15));
						setForeground(Color.blue);
						setText("   (" + acc + ")");
						setSize(getPreferredSize());

						animMonitor.updateMTUs(this.fragments);
					}
				}
				runAnimation(src, trg, animMonitor);
			}
		}

		if (!err) {
			if (animMonitor.shouldStop) {
				animMonitor.setError("Simulation stopped due to user request");
			} else if (this.path.get(this.path.size() - 1) instanceof Network) {
				animMonitor
						.setError("Host can not be reached: check routing tables settings.");
				err = true;
			} else if (this.path.get(this.path.size() - 1) instanceof Router) {
				animMonitor
						.setError("Message dropped by router: no matching lines in routing table");
				err = true;
			}

		}

		animMonitor.hideStopSim();
		animMonitor.setLegend(null, this);

		JOptionPane.showMessageDialog(board, "Simulation"
				+ ((animMonitor.shouldStop) ? " interrupted" : " completed")
				+ ((err) ? " with errors" : ""), "Message", (err) ? 0 : 1);

		setText("");
		setIcon(this.main);
		setSize(getPreferredSize());

		setLocation(origLoc);
		board.repaint();
		board.messagePanel.remove(animMonitor);

		board.messagePanel.updateUI();
		board.updateUI();
		board.addMouseListener(board.mouseListener);
	}

	public void runAnimation(Component src, Component trg,
			AnimationMonitor animMonitor) {
		MainDrawBoard board = MainDrawBoard.staticInstance;

		boolean overItem = false;

		int sourceX = src.getX() + src.getWidth() / 2;
		int sourceY = src.getY() + src.getHeight() / 2;
		int targetX = trg.getX() + trg.getWidth() / 2;
		int targetY = trg.getY() + trg.getHeight() / 2;

		double x = sourceX;
		double y = sourceY;

		if (targetX != sourceX) {
			double a = (targetY - sourceY) / (targetX - sourceX);
			double b = sourceY - a * sourceX;

			int sign = (targetX > sourceX) ? 1 : -1;
			do {
				do {
					x += 1 * sign;
					y = (int) (a * x + b);

					Component overComp = board.getComponentAt((int) x, (int) y);
					if ((overComp instanceof Router)
							|| (overComp instanceof Network)) {
						if (!overItem) {
							overItem = true;
							animMonitor.setLegend(overComp, this);
							animMonitor.repaint();
						}

					} else if (overItem) {
						overItem = false;
					}

					setLocation((int) x, (int) y);
					board.repaint();
					try {
						Thread.sleep(30L);
					} catch (InterruptedException e) {
						e.printStackTrace();
					}
				} while ((sign == 1) && (x <= targetX));
				if (sign != -1)
					return;
			} while (x >= targetX);
		} else {
			int sign = (targetY > sourceY) ? 1 : -1;
			while (((sign == 1) && (y <= targetY))
					|| ((sign == -1) && (y >= targetY))) {
				y += 1 * sign;

				setLocation((int) x, (int) y);
				board.repaint();
				try {
					Thread.sleep(40L);
				} catch (InterruptedException e) {
					e.printStackTrace();
				}
			}
		}
	}

	public void run() {
		try {
			Thread.sleep(500L);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		runAnimation();
	}
}

/*
 * Location: C:\Users\MT\Desktop\mtutr.jar Qualified Name:
 * mtutr.objects.Datagram JD-Core Version: 0.5.4
 */
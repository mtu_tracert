package mtutr.objects;

import mtutr.common.IPAddress;

public class RouteTestResult implements Comparable {
	public int bits;
	public IPAddress ifc;
	public IPAddress nextHop;
	public Network network;
	public RoutingTableEntry rte;

	public RouteTestResult(RoutingTableEntry r) {
		this.rte = r;
	}

	public int compareTo(Object o) {
		if (o instanceof RouteTestResult) {
			RouteTestResult r = (RouteTestResult) o;
			return this.bits - r.bits;
		}

		System.out.print("Invalid object");
		return -1;
	}
}

/*
 * Location: C:\Users\MT\Desktop\mtutr.jar Qualified Name:
 * mtutr.objects.RouteTestResult JD-Core Version: 0.5.4
 */
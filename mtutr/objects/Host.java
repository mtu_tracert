package mtutr.objects;

import java.awt.Component;

import javax.swing.JLabel;

import mtutr.common.IPAddress;

public class Host extends JLabel implements IPKitItem {
	private static final long serialVersionUID = 1L;
	public Network network = null;

	public Host(String string, Network net) {
		super(string);
		this.network = net;
	}

	public IPAddress getIP() {
		return this.network.allocateIP(this);
	}

	public void dispatchEvent() {
	}

	public Component getjComponent() {
		return this;
	}
}

/*
 * Location: C:\Users\MT\Desktop\mtutr.jar Qualified Name: mtutr.objects.Host
 * JD-Core Version: 0.5.4
 */
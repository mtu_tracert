package mtutr.objects;

import java.awt.Component;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.ListIterator;

import javax.swing.JLabel;

import mtutr.common.IPAddress;

public class Router extends JLabel implements IPKitItem {
	private static final long serialVersionUID = 1L;
	public List networks = new LinkedList();
	public RoutingTable routingTable = new RoutingTable(this);

	public Router(String string) {
		super(string);
	}

	public IPAddress getIP(Network net) {
		return net.allocateIP(this);
	}

	public void dispatchEvent() {
	}

	private Network findNetworkByIP(String a) {
		try {
			IPAddress ip = new IPAddress(a);
			Iterator it = this.networks.iterator();
			while (it.hasNext()) {
				Network net = null;
				net = (Network) it.next();
				if (net.getIP().equals(ip)) {
					return net;
				}
			}
			return null;
		} catch (Throwable e) {
		}
		return null;
	}

	public boolean cleanRoutingTable() {
		if ((this.networks.size() == 0)
				&& (this.routingTable.entries.size() != 0)) {
			this.routingTable.entries.clear();
			return true;
		}

		boolean rtChanged = false;
		Iterator it = this.routingTable.entries.iterator();
		RoutingTableEntry rte = null;
		while (it.hasNext()) {
			rte = (RoutingTableEntry) it.next();
			if (findNetworkByIP(rte.ifc) == null) {
				it.remove();
				rtChanged = true;
			}
		}

		return rtChanged;
	}

	private boolean cleanRoutingTableByNet(Network net) {
		boolean rtChanged = false;
		IPAddress netIP = net.getIP();
		if (netIP == null) {
			return false;
		}
		ListIterator it = this.routingTable.entries.listIterator();
		while (it.hasNext()) {
			RoutingTableEntry rte = (RoutingTableEntry) it.next();
			if (netIP.equals(rte.ifc)) {
				it.remove();
				rtChanged = true;
			}
		}
		return rtChanged;
	}

	public boolean detachFromNetwork(Network net) {
		if (this.networks.contains(net)) {
			this.networks.remove(net);
		}

		return cleanRoutingTableByNet(net);
	}

	public Network getNetwork(IPAddress ifc) {
		Iterator it = this.networks.iterator();

		while (it.hasNext()) {
			Network network = (Network) it.next();
			if (network.getIP().equals(ifc)) {
				return network;
			}
		}
		return null;
	}

	public Component getjComponent() {
		return this;
	}
}

/*
 * Location: C:\Users\MT\Desktop\mtutr.jar Qualified Name: mtutr.objects.Router
 * JD-Core Version: 0.5.4
 */
package mtutr.objects;

import mtutr.common.IPAddress;

public class RoutingTableEntry {
	public String net = "";
	public String ifc = "";
	public String gateway = "";
	public String cidr = "";

	public String net_backup = "";
	public String ifc_backup = "";
	public String gateway_backup = "";
	public String cidr_backup = "";

	public boolean isNew = true;

	public boolean isValid() {
		try {
			Integer.parseInt(this.cidr);
		} catch (Throwable e) {
			if (!IPAddress.isValidAddress(this.cidr)) {
				return false;
			}

		}

		return (IPAddress.isValidAddress(this.net))
				&& (IPAddress.isValidAddress(this.ifc))
				&& (IPAddress.isValidAddress(this.gateway));
	}

	public void backup() {
		this.net_backup = this.net;
		this.ifc_backup = this.ifc;
		this.gateway_backup = this.gateway;
		this.cidr_backup = this.cidr;
	}

	public void rollback() {
		this.net = this.net_backup;
		this.ifc = this.ifc_backup;
		this.gateway = this.gateway_backup;
		this.cidr = this.cidr_backup;
	}

	public void commit() {
		this.isNew = false;
	}

	public RouteTestResult findMatch(IPAddress ip) {
		IPAddress netIP = null;
		try {
			netIP = new IPAddress(this.net);
		} catch (Throwable e) {
			return null;
		}

		int intCIDR = -1;
		boolean cont = false;
		try {
			intCIDR = Integer.parseInt(this.cidr);
			if (ip.getNetworkIPByCIDR(intCIDR).equals(netIP))
				cont = true;
		} catch (Throwable e) {
			long netmask = -1L;
			try {
				netmask = new IPAddress(this.cidr).getIPLong();
			} catch (Throwable ee) {
				return null;
			}

			if (new IPAddress(ip.getIPLong() & netmask).equals(netIP)) {
				cont = true;
				try {
					intCIDR = new IPAddress(this.cidr).getMinimalMask();
				} catch (Throwable ee) {
					return null;
				}
			}

		}

		if (cont) {
			RouteTestResult rtr = new RouteTestResult(this);
			try {
				rtr.ifc = new IPAddress(this.ifc);
				rtr.nextHop = new IPAddress(this.gateway);
			} catch (Throwable e) {
				return null;
			}
			rtr.bits = intCIDR;
			return rtr;
		}

		return null;
	}

	@Override
	public String toString() {
		return this.net + " / " + this.cidr + " / " + this.gateway + " / "
				+ this.net;
	}
}

/*
 * Location: C:\Users\MT\Desktop\mtutr.jar Qualified Name:
 * mtutr.objects.RoutingTableEntry JD-Core Version: 0.5.4
 */
/*    */ package mtutr.listeners;
/*    */ 
/*    */ /*    */ import java.awt.Color;
/*    */ import java.awt.Component;
/*    */ import java.awt.event.MouseEvent;
/*    */ import java.awt.event.MouseListener;
/*    */ import java.util.List;
/*    */ import javax.swing.BorderFactory;
/*    */ import javax.swing.JLabel;
import javax.swing.JOptionPane;

import mtutr.MainDrawBoard;
import mtutr.objects.Datagram;
import mtutr.objects.Host;
import mtutr.objects.IPKitItem;
import mtutr.objects.Network;
import mtutr.objects.Router;
/*    */ 
/*    */ public class SelectionMouseListener
/*    */   implements MouseListener
/*    */ {
/* 20 */   MainDrawBoard board = null;
/*    */   public Class selectionClass;
/* 22 */   public IPKitItem selectedObject = null;
/*    */   public boolean source;
/*    */ 
/*    */   public SelectionMouseListener(MainDrawBoard theBoard)
/*    */   {
/* 26 */     this.board = theBoard;
/*    */   }
/*    */ 
/*    */   public void mouseClicked(MouseEvent event) {
/* 30 */     Component comp = this.board.getComponentAt(event.getPoint());
/* 31 */     if (this.selectedObject instanceof Router) {
/* 32 */       Router router = (Router)this.selectedObject;
/* 33 */       if (comp.getClass().equals(this.selectionClass)) {
/* 34 */         Network net = (Network)comp;
/* 35 */         if (net.getIP() == null) {
/* 36 */           JOptionPane.showMessageDialog(this.board, "Unable connecting to router while network's IP addresses haven't defined.\nDefine network's IP addresses before connecting it to a router.", 
/* 37 */             "Warning", 2);
/* 38 */           return;
/*    */         }
/*    */ 
/* 41 */         if (router.networks.contains(comp)) {
/* 42 */           ((JLabel)comp).setBorder(BorderFactory.createEmptyBorder());
/* 43 */           router.networks.remove(comp);
/* 44 */           net.routers.remove(this.selectedObject);
/* 45 */           net.deAllocateIP(this.selectedObject);
/*    */         }
/* 48 */         else if (net.allocateIP(this.selectedObject) != null) {
/* 49 */           net.setBorder(BorderFactory.createLineBorder(Color.GREEN));
/* 50 */           router.networks.add(comp);
/* 51 */           net.routers.add(this.selectedObject);
/* 52 */           if (net.routers.size() == 1)
/* 53 */             net.defaultGateway = ((Router)this.selectedObject);
/*    */         }
/*    */         else
/*    */         {
/* 57 */           JOptionPane.showMessageDialog(this.board, "Unable to connect router to selected network: IP addresses pool is empty.");
/*    */         }
/*    */ 
/*    */       }
/*    */ 
/*    */     }
/* 63 */     else if (this.selectedObject instanceof Datagram) {
/* 64 */       if (comp instanceof Host) {
/* 65 */         Datagram dg = (Datagram)this.selectedObject;
/*    */ 
/* 67 */         Host host1 = (this.source) ? dg.getSource() : dg.getTarget();
/* 68 */         if (host1 != null) {
/* 69 */           host1.setBorder(BorderFactory.createEmptyBorder());
/*    */         }
/*    */ 
/* 72 */         Host host2 = (Host)comp;
/* 73 */         if (host2.equals(host1))
/*    */         {
/* 75 */           host2.setBorder(BorderFactory.createEmptyBorder());
/* 76 */           host2 = null;
/*    */         }
/*    */         else
/*    */         {
/* 80 */           host2.setBorder(BorderFactory.createLineBorder(Color.GREEN));
/*    */         }
/*    */ 
/* 83 */         if (this.source) {
/* 84 */           dg.setSource(host2);
/*    */         }
/*    */         else {
/* 87 */           dg.setTarget(host2);
/*    */         }
/*    */       }
/*    */     }
/* 91 */     else if (this.selectedObject instanceof Network) {
/* 92 */       Network net = (Network)this.selectedObject;
/* 93 */       Router defaultGateway = net.defaultGateway;
/* 94 */       if (defaultGateway != null) {
/* 95 */         defaultGateway.setBorder(BorderFactory.createLineBorder(Color.GREEN));
/*    */       }
/* 97 */       if (comp instanceof Router) {
/* 98 */         Router r = (Router)comp;
/* 99 */         if (net.routers.contains(r)) {
/* 100 */           if (defaultGateway != null) {
/* 101 */             defaultGateway.setBorder(BorderFactory.createLineBorder(Color.BLUE));
/*    */           }
/* 103 */           r.setBorder(BorderFactory.createLineBorder(Color.GREEN));
/* 104 */           net.defaultGateway = r;
/*    */         }
/*    */       }
/*    */     }
/*    */   }
/*    */ 
/*    */   public void mouseEntered(MouseEvent arg0)
/*    */   {
/*    */   }
/*    */ 
/*    */   public void mouseExited(MouseEvent arg0)
/*    */   {
/*    */   }
/*    */ 
/*    */   public void mousePressed(MouseEvent arg0)
/*    */   {
/*    */   }
/*    */ 
/*    */   public void mouseReleased(MouseEvent arg0)
/*    */   {
/*    */   }
/*    */ }

/* Location:           C:\Users\MT\Desktop\mtutr.jar
 * Qualified Name:     mtutr.listeners.SelectionMouseListener
 * JD-Core Version:    0.5.4
 */
/*     */ package mtutr.listeners;
/*     */ 
/*     */ /*     */ import java.awt.Component;
/*     */ import java.awt.event.ActionEvent;
/*     */ import java.awt.event.ActionListener;
/*     */ import java.awt.event.MouseEvent;
/*     */ import java.util.List;
import javax.swing.JOptionPane;

import mtutr.MainDrawBoard;
import mtutr.common.BoardCleaner;
import mtutr.objects.Datagram;
import mtutr.objects.Host;
import mtutr.objects.IPKitItem;
import mtutr.objects.Network;
import mtutr.objects.Router;
import mtutr.windows.DatagramProperties;
import mtutr.windows.NetworkProperties;
import mtutr.windows.RouterProperties;
/*     */ 
/*     */ public class MenuListener
/*     */   implements ActionListener
/*     */ {
/*     */   private MainDrawBoard board;
/*     */   private MouseEvent mouseEvent;
/*  24 */   private Component selectedComponent = null;
/*  25 */   private IPKitItem item = null;
/*     */ 
/*     */   public MenuListener(MainDrawBoard newBoard) {
/*  28 */     this.board = newBoard;
/*     */   }
/*     */ 
/*     */   public void setSelectedComponent(MouseEvent newItem) {
/*  32 */     this.mouseEvent = newItem;
/*  33 */     this.selectedComponent = this.board.getComponentAt(this.mouseEvent.getPoint());
/*     */   }
/*     */ 
/*     */   public void actionPerformed(ActionEvent event) {
/*  37 */     if (event.getActionCommand().equalsIgnoreCase("Add Network")) {
/*  38 */       Network network = this.board.createNewNetwork();
/*  39 */       this.item = network;
/*  40 */       this.board.networks.add(network);
/*  41 */       network.setLocation(this.mouseEvent.getPoint());
/*  42 */       this.board.add(network, 0);
/*     */     }
/*  44 */     else if (event.getActionCommand().equalsIgnoreCase("Remove Network")) {
/*  45 */       this.board.removeNetwork((Network)this.selectedComponent);
/*     */     }
/*  47 */     else if (event.getActionCommand().equalsIgnoreCase("Add Router")) {
/*  48 */       Router router = this.board.createNewRouter();
/*  49 */       this.item = router;
/*  50 */       router.setLocation(this.mouseEvent.getPoint());
/*  51 */       this.board.routers.add(router);
/*  52 */       this.board.add(router, 0);
/*  53 */     }if (event.getActionCommand().equalsIgnoreCase("Add Host")) {
/*  54 */       Network net = (Network)this.selectedComponent;
/*  55 */       if (net.getIP() == null) {
/*  56 */         JOptionPane.showMessageDialog(this.board, "Unable adding a host while network's IP addresses haven't defined.\nDefine network's IP addresses before adding a host to it.");
/*     */ 
/*  58 */         return;
/*     */       }
/*  60 */       Host host = this.board.createNewHost(net);
/*  61 */       this.item = host;
/*  62 */       if (net.allocateIP(host) == null) {
/*  63 */         JOptionPane.showMessageDialog(this.board, "Unable adding new host: IP addresses pool is empty.");
/*     */       }
/*     */       else
/*     */       {
/*  67 */         ((Network)this.selectedComponent).hosts.add(host);
/*  68 */         host.setLocation(this.mouseEvent.getPoint());
/*  69 */         this.board.add(host, 0);
/*     */       }
/*     */     }
/*  72 */     else if (event.getActionCommand().equalsIgnoreCase("Edit Network")) {
/*  73 */       NetworkProperties tmp = new NetworkProperties((Network)this.selectedComponent);
/*  74 */       tmp.show();
/*     */     }
/*  76 */     else if (event.getActionCommand().equalsIgnoreCase("Edit Routing Table")) {
/*  77 */       RouterProperties tmp = new RouterProperties((Router)this.selectedComponent);
/*  78 */       tmp.show();
/*     */     }
/*  80 */     else if (event.getActionCommand().equalsIgnoreCase("Set Default Gateway")) {
/*  81 */       this.board.enterSelectionMode((Network)this.selectedComponent);
/*     */     }
/*  83 */     else if (event.getActionCommand().equalsIgnoreCase("Remove Router")) {
/*  84 */       this.board.removeRouter((Router)this.selectedComponent);
/*     */     }
/*  86 */     else if (event.getActionCommand().equalsIgnoreCase("Remove Host")) {
/*  87 */       this.board.removeHost((Host)this.selectedComponent);
/*     */     }
/*  89 */     else if (event.getActionCommand().equalsIgnoreCase("Connect to networks")) {
/*  90 */       this.board.enterSelectionMode((Router)this.selectedComponent);
/*     */     }
/*  92 */     else if (event.getActionCommand().equalsIgnoreCase("Add Datagram")) {
/*  93 */       Datagram datagram = this.board.createNewDatagram();
/*  94 */       this.item = datagram;
/*  95 */       this.board.datagrams.add(datagram);
/*  96 */       datagram.setLocation(this.mouseEvent.getPoint());
/*  97 */       this.board.add(datagram, 0);
/*     */     }
/*  99 */     else if (event.getActionCommand().equalsIgnoreCase("Send Datagram")) {
/* 100 */       Datagram d = (Datagram)this.selectedComponent;
/* 101 */       d.send();
/*     */     }
/* 103 */     else if (event.getActionCommand().equalsIgnoreCase("Edit Datagram")) {
/* 104 */       new DatagramProperties((Datagram)this.selectedComponent);
/*     */     }
/* 106 */     else if (event.getActionCommand().equalsIgnoreCase("Remove Datagram")) {
/* 107 */       this.board.removeDatagram((Datagram)this.selectedComponent);
/*     */     }
/* 109 */     else if (event.getActionCommand().equalsIgnoreCase("Set Source & Target")) {
/* 110 */       this.board.sourceAndTarget = true;
/* 111 */       this.board.enterSelectionMode((Datagram)this.selectedComponent, true);
/*     */     }
/* 113 */     else if (event.getActionCommand().equalsIgnoreCase("Set Source Only")) {
/* 114 */       this.board.enterSelectionMode((Datagram)this.selectedComponent, true);
/*     */     }
/* 116 */     else if (event.getActionCommand().equalsIgnoreCase("Set Target Only")) {
/* 117 */       this.board.enterSelectionMode((Datagram)this.selectedComponent, false);
/*     */     }
/* 119 */     else if (event.getActionCommand().equalsIgnoreCase("Remove All"))
/*     */     {
/* 121 */       new Thread(new BoardCleaner(this.board)).start();
/* 122 */       return;
/*     */     }
/*     */ 
/* 125 */     this.board.repaint();
/*     */   }
/*     */ 
/*     */   public IPKitItem getLastItem() {
/* 129 */     return this.item;
/*     */   }
/*     */ }

/* Location:           C:\Users\MT\Desktop\mtutr.jar
 * Qualified Name:     mtutr.listeners.MenuListener
 * JD-Core Version:    0.5.4
 */
package mtutr.listeners;

import java.awt.Component;
import java.awt.Point;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.MouseMotionListener;

import javax.swing.JLabel;

import mtutr.MainDrawBoard;
import mtutr.objects.Datagram;
import mtutr.objects.Host;
import mtutr.objects.Network;
import mtutr.objects.Router;
import mtutr.windows.DatagramProperties;
import mtutr.windows.NetworkProperties;
import mtutr.windows.RouterProperties;

public class MainMouseListener implements MouseListener, MouseMotionListener {
	private boolean inDrag = false;
	int dX;
	int dY;
	MainDrawBoard board;
	Component draggedComp = null;
	boolean selectMode;
	Class selectionClass;

	public MainMouseListener(MainDrawBoard val) {
		this.board = val;
	}

	public void mouseClicked(MouseEvent event) {
		if (event.getButton() == 3) {
			if (this.board.firstClick) {
				this.board.firstClick = false;
//				this.board.remove(this.board.startLabel);
				this.board.updateUI();
			}

			this.board.menuListener.setSelectedComponent(event);
			Component comp = this.board.getComponentAt(event.getPoint());

			if (comp instanceof Network) {
				if (((Network) comp).getIP() == null) {
					this.board.addHost.setEnabled(false);
				} else {
					this.board.addHost.setEnabled(true);
				}
				this.board.network_menu.show(this.board, event.getX(), event
						.getY());
			} else if (comp instanceof Datagram) {
				if ((((Datagram) comp).getTarget() == null)
						|| (((Datagram) comp).getSource() == null))
					this.board.sendDatagram.setEnabled(false);
				else {
					this.board.sendDatagram.setEnabled(true);
				}
				this.board.datagram_menu.show(this.board, event.getX(), event
						.getY());
			} else if (comp instanceof Router) {
				this.board.router_menu.show(this.board, event.getX(), event
						.getY());
			} else if (comp instanceof Host) {
				this.board.host_menu.show(this.board, event.getX(), event
						.getY());
			} else {
				this.board.main_menu.show(this.board, event.getX(), event
						.getY());
			}
		} else if ((event.getButton() == 1) && (event.getClickCount() == 2)) {
			Component comp = this.board.getComponentAt(event.getPoint());
			if (comp instanceof Network) {
				new NetworkProperties((Network) comp).setVisible(true);
			}

			if (comp instanceof Datagram) {
				new DatagramProperties((Datagram) comp).setVisible(true);
			}

			if (comp instanceof Router) {
				new RouterProperties((Router) comp).setVisible(true);
			}
		}
	}

	public void mouseEntered(MouseEvent arg0) {
	}

	public void mouseExited(MouseEvent arg0) {
	}

	public void mousePressed(MouseEvent event) {
		Point p = event.getPoint();
		Component comp = this.board.getComponentAt(event.getPoint());

		if ((comp instanceof JLabel)
				&& (((JLabel) comp).getText()
						.equals("Right-click to get started"))) {
			return;
		}

		if ((!this.inDrag) && (comp != null) && (comp != this.board)) {
			this.inDrag = true;
			this.draggedComp = comp;
			this.board.remove(comp);
			this.board.add(comp, 0);
			this.dX = (comp.getX() - p.x);
			this.dY = (comp.getY() - p.y);
		}
	}

	public void mouseReleased(MouseEvent arg0) {
		this.inDrag = false;
		this.draggedComp = null;
	}

	public void mouseDragged(MouseEvent event) {
		Point p = event.getPoint();
		if ((!this.inDrag) || (p.x <= 0) || (p.x >= this.board.getWidth())
				|| (p.y <= 0) || (p.y >= this.board.getHeight()))
			return;
		this.draggedComp.setLocation(this.dX + p.x, this.dY + p.y);
		this.board.repaint();
	}

	public void mouseMoved(MouseEvent arg0) {
	}
}

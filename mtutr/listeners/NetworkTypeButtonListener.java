/**
 * 
 */
package mtutr.listeners;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import mtutr.MainDrawBoard;

/**
 * @author New user
 *
 */
public class NetworkTypeButtonListener implements ActionListener {


	private final MainDrawBoard mainDrawBoard;
	private final boolean isIPv6;
	
	public NetworkTypeButtonListener(MainDrawBoard mainDrawBoard, boolean isIPv6) {
		super();
		this.mainDrawBoard = mainDrawBoard;
		this.isIPv6 = isIPv6;
	}

	/* (non-Javadoc)
	 * @see java.awt.event.ActionListener#actionPerformed(java.awt.event.ActionEvent)
	 */
	@Override
	public void actionPerformed(ActionEvent e) {
		mainDrawBoard.setIPv6(isIPv6);
		this.mainDrawBoard.removeAll();
		this.mainDrawBoard.updateUI();

	}

}

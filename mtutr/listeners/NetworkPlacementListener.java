/*     */ package mtutr.listeners;
/*     */ 
/*     */ /*     */ import java.awt.Component;
/*     */ import java.awt.Point;
/*     */ import java.awt.event.ActionEvent;
/*     */ import java.awt.event.ActionListener;
/*     */ import java.awt.event.MouseEvent;
/*     */ import java.util.LinkedList;
/*     */ import java.util.List;
import javax.swing.JOptionPane;

import mtutr.MainDrawBoard;
import mtutr.common.IPAddress;
import mtutr.objects.Datagram;
import mtutr.objects.Host;
import mtutr.objects.Network;
import mtutr.objects.Router;
import mtutr.objects.RoutingTable;
import mtutr.objects.RoutingTableEntry;
/*     */ 
/*     */ public class NetworkPlacementListener
/*     */   implements ActionListener
/*     */ {
/*     */   private MainDrawBoard board;
/*     */   private MouseEvent mouseEvent;
/*  22 */   private Component selectedComponent = null;
/*  23 */   private MenuListener menuListener = null;
/*     */ 
/*     */   public NetworkPlacementListener(MainDrawBoard newBoard, MenuListener newMenuListener) {
/*  26 */     this.board = newBoard;
/*  27 */     this.menuListener = newMenuListener;
/*     */   }
/*     */ 
/*     */   public void setSelectedComponent(MouseEvent newItem) {
/*  31 */     this.mouseEvent = newItem;
/*  32 */     this.selectedComponent = this.board.getComponentAt(this.mouseEvent.getPoint());
/*     */   }
/*     */ 
/*     */   public void actionPerformed(ActionEvent event) {
/*  36 */     if (event.getActionCommand().equalsIgnoreCase("Star")) {
/*  37 */       placeNetwork1();
/*     */     }
/*  39 */     else if (event.getActionCommand().equalsIgnoreCase("Loop")) {
/*  40 */       placeNetwork2();
/*     */     }
/*  42 */     else if (event.getActionCommand().equalsIgnoreCase("Long"))
/*  43 */       placeNetwork3();
/*     */   }
/*     */ 
/*     */   public void placeNetwork1()
/*     */   {
/*     */     try {
/*  49 */       Router mainRouter = placeRouter(215, 320);
/*     */ 
/*  51 */       RoutingTableEntry rte = new RoutingTableEntry();
/*  52 */       rte.net = "1.0.0.0"; rte.cidr = "8"; rte.gateway = "1.0.0.2"; rte.ifc = "1.0.0.0"; rte.isNew = false;
/*  53 */       mainRouter.routingTable.entries.add(rte);
/*     */ 
/*  55 */       rte = new RoutingTableEntry();
/*  56 */       rte.net = "2.0.0.0"; rte.cidr = "8"; rte.gateway = "2.0.0.2"; rte.ifc = "2.0.0.0"; rte.isNew = false;
/*  57 */       mainRouter.routingTable.entries.add(rte);
/*     */ 
/*  59 */       rte = new RoutingTableEntry();
/*  60 */       rte.net = "3.0.0.0"; rte.cidr = "8"; rte.gateway = "3.0.0.2"; rte.ifc = "3.0.0.0"; rte.isNew = false;
/*  61 */       mainRouter.routingTable.entries.add(rte);
/*     */ 
/*  63 */       Network net1 = placeNetwork(70, 250);
/*  64 */       net1.setCIDR_IP(new IPAddress("1.0.0.0"), 16);
/*  65 */       Router router1 = placeRouter(85, 150);
/*  66 */       Network net11 = placeNetwork(0, 0);
/*  67 */       net11.setCIDR_IP(new IPAddress("1.1.0.0"), 24);
/*  68 */       Network net12 = placeNetwork(140, 0);
/*  69 */       net12.setCIDR_IP(new IPAddress("1.2.0.0"), 24);
/*  70 */       connectNetworkToRouter(net1, mainRouter);
/*  71 */       connectNetworkToRouter(net1, router1);
/*  72 */       connectNetworkToRouter(net11, router1);
/*  73 */       connectNetworkToRouter(net12, router1);
/*  74 */       placeHost(net11, 11, 58);
/*  75 */       placeHost(net11, 100, 58);
/*  76 */       placeHost(net12, 201, 58);
/*     */ 
/*  78 */       rte = new RoutingTableEntry();
/*  79 */       rte.net = "1.1.0.0"; rte.cidr = "24"; rte.gateway = "1.1.0.1"; rte.ifc = "1.1.0.0"; rte.isNew = false;
/*  80 */       router1.routingTable.entries.add(rte);
/*     */ 
/*  82 */       rte = new RoutingTableEntry();
/*  83 */       rte.net = "1.2.0.0"; rte.cidr = "24"; rte.gateway = "1.2.0.1"; rte.ifc = "1.2.0.0"; rte.isNew = false;
/*  84 */       router1.routingTable.entries.add(rte);
/*     */ 
/*  86 */       rte = new RoutingTableEntry();
/*  87 */       rte.net = "0.0.0.0"; rte.cidr = "0"; rte.gateway = "1.0.0.1"; rte.ifc = "1.0.0.0"; rte.isNew = false;
/*  88 */       router1.routingTable.entries.add(rte);
/*     */ 
/*  92 */       Network net2 = placeNetwork(330, 250);
/*  93 */       net2.setCIDR_IP(new IPAddress("2.0.0.0"), 16);
/*  94 */       Router router2 = placeRouter(340, 150);
/*  95 */       Network net21 = placeNetwork(260, 0);
/*  96 */       net21.setCIDR_IP(new IPAddress("2.1.0.0"), 24);
/*  97 */       Network net22 = placeNetwork(380, 0);
/*  98 */       net22.setCIDR_IP(new IPAddress("2.2.0.0"), 24);
/*  99 */       connectNetworkToRouter(net2, mainRouter);
/* 100 */       connectNetworkToRouter(net2, router2);
/* 101 */       connectNetworkToRouter(net21, router2);
/* 102 */       connectNetworkToRouter(net22, router2);
/* 103 */       placeHost(net21, 291, 58);
/* 104 */       placeHost(net22, 455, 58);
/*     */ 
/* 107 */       rte = new RoutingTableEntry();
/* 108 */       rte.net = "2.1.0.0"; rte.cidr = "24"; rte.gateway = "2.1.0.1"; rte.ifc = "2.1.0.0"; rte.isNew = false;
/* 109 */       router2.routingTable.entries.add(rte);
/*     */ 
/* 111 */       rte = new RoutingTableEntry();
/* 112 */       rte.net = "2.2.0.0"; rte.cidr = "24"; rte.gateway = "2.2.0.1"; rte.ifc = "2.2.0.0"; rte.isNew = false;
/* 113 */       router2.routingTable.entries.add(rte);
/*     */ 
/* 115 */       rte = new RoutingTableEntry();
/* 116 */       rte.net = "0.0.0.0"; rte.cidr = "0"; rte.gateway = "2.0.0.1"; rte.ifc = "2.0.0.0"; rte.isNew = false;
/* 117 */       router2.routingTable.entries.add(rte);
/*     */ 
/* 120 */       Network net3 = placeNetwork(195, 389);
/* 121 */       net3.setCIDR_IP(new IPAddress("3.0.0.0"), 16);
/* 122 */       Router router3 = placeRouter(210, 480);
/* 123 */       Network net31 = placeNetwork(13, 529);
/* 124 */       net31.setCIDR_IP(new IPAddress("3.1.0.0"), 24);
/* 125 */       Network net32 = placeNetwork(378, 529);
/* 126 */       net32.setCIDR_IP(new IPAddress("3.2.0.0"), 24);
/* 127 */       connectNetworkToRouter(net3, mainRouter);
/* 128 */       connectNetworkToRouter(net3, router3);
/* 129 */       connectNetworkToRouter(net31, router3);
/* 130 */       connectNetworkToRouter(net32, router3);
/* 131 */       placeHost(net31, 139, 549);
/* 132 */       placeHost(net31, 13, 484);
/* 133 */       placeHost(net32, 318, 549);
/* 134 */       placeHost(net32, 443, 484);
/*     */ 
/* 136 */       rte = new RoutingTableEntry();
/* 137 */       rte.net = "3.1.0.0"; rte.cidr = "24"; rte.gateway = "3.1.0.1"; rte.ifc = "3.1.0.0"; rte.isNew = false;
/* 138 */       router3.routingTable.entries.add(rte);
/*     */ 
/* 140 */       rte = new RoutingTableEntry();
/* 141 */       rte.net = "3.2.0.0"; rte.cidr = "24"; rte.gateway = "3.2.0.1"; rte.ifc = "3.2.0.0"; rte.isNew = false;
/* 142 */       router3.routingTable.entries.add(rte);
/*     */ 
/* 144 */       rte = new RoutingTableEntry();
/* 145 */       rte.net = "0.0.0.0"; rte.cidr = "0"; rte.gateway = "3.0.0.1"; rte.ifc = "3.0.0.0"; rte.isNew = false;
/* 146 */       router3.routingTable.entries.add(rte);
/*     */ 
/* 148 */       placeDatagram(221, 221);
/*     */ 
/* 150 */       this.board.repaint();
/* 151 */       JOptionPane.showMessageDialog(this.board, 
/* 152 */         "<html><b>Star</b> - This network is constructed of three main networks connected using<br> \t\t<br>a router to each other. Each of these main networks is connected to another two<br> \t\t<br>\"sub-networks\".<br> \t\t<br>Hosts are allocated only in the \"sub-networks\".<br> \t\t<br>All routers are already configured to transfer datagrams between the hosts.<br> \t\t<br>Determine datagram's source and target, and send it</html>");
/*     */     }
/*     */     catch (Throwable e)
/*     */     {
/* 160 */       return;
/*     */     }
/*     */   }
/*     */ 
/*     */   public void placeNetwork2() {
/*     */     try {
/* 166 */       Network net1 = placeNetwork(22, 292);
/* 167 */       net1.setCIDR_IP(new IPAddress("1.0.0.0"), 16);
/* 168 */       Network net2 = placeNetwork(324, 292);
/* 169 */       net2.setCIDR_IP(new IPAddress("2.0.0.0"), 16);
/* 170 */       Router router1 = placeRouter(206, 84);
/* 171 */       Router router2 = placeRouter(206, 489);
/* 172 */       Host h1 = placeHost(net1, 162, 310);
/* 173 */       Host h2 = placeHost(net2, 264, 310);
/* 174 */       connectNetworkToRouter(net1, router1);
/* 175 */       connectNetworkToRouter(net2, router2);
/* 176 */       connectNetworkToRouter(net1, router2);
/* 177 */       connectNetworkToRouter(net2, router1);
/*     */ 
/* 179 */       RoutingTableEntry rte = new RoutingTableEntry();
/* 180 */       rte.net = "2.0.0.0";
/* 181 */       rte.cidr = "255.0.0.0";
/* 182 */       rte.gateway = "2.0.0.2";
/* 183 */       rte.ifc = "2.0.0.0";
/* 184 */       rte.isNew = false;
/* 185 */       router1.routingTable.entries.add(rte);
/* 186 */       rte = new RoutingTableEntry();
/* 187 */       rte.net = "1.0.0.0";
/* 188 */       rte.cidr = "255.0.0.0";
/* 189 */       rte.gateway = "1.0.0.3";
/* 190 */       rte.ifc = "1.0.0.0";
/* 191 */       rte.isNew = false;
/* 192 */       router1.routingTable.entries.add(rte);
/*     */ 
/* 195 */       rte = new RoutingTableEntry();
/* 196 */       rte.net = "1.0.0.0";
/* 197 */       rte.cidr = "255.0.0.0";
/* 198 */       rte.gateway = "1.0.0.2";
/* 199 */       rte.ifc = "1.0.0.0";
/* 200 */       rte.isNew = false;
/* 201 */       router2.routingTable.entries.add(rte);
/* 202 */       rte = new RoutingTableEntry();
/* 203 */       rte.net = "2.0.0.0";
/* 204 */       rte.cidr = "255.0.0.0";
/* 205 */       rte.gateway = "2.0.0.3";
/* 206 */       rte.ifc = "2.0.0.0";
/* 207 */       rte.isNew = false;
/* 208 */       router2.routingTable.entries.add(rte);
/*     */ 
/* 210 */       Datagram d = placeDatagram(221, 221);
/* 211 */       d.setTTL(3);
/* 212 */       d.setSource(h1);
/* 213 */       d.setTarget(h2);
/*     */ 
/* 215 */       this.board.repaint();
/* 216 */       JOptionPane.showMessageDialog(this.board, 
/* 217 */         "<html><b>Loop</b> - This network simulates an error in the configuration of the routers.<br> \t\t<br>The source and the target of the datagram are already determined.<br> \t\t<br>Simply send the datagram and watch what happens.<br></html>");
/*     */     }
/*     */     catch (Throwable e)
/*     */     {
/* 223 */       return;
/*     */     }
/*     */   }
/*     */ 
/*     */   public void placeNetwork3() {
/*     */     try {
/* 228 */       Network net1 = placeNetwork(0, 0);
/* 229 */       net1.setCIDR_IP(new IPAddress("1.0.0.0"), 8);
/* 230 */       Router router1 = placeRouter(32, 120);
/* 231 */       connectNetworkToRouter(net1, router1);
/* 232 */       placeHost(net1, 150, 12);
/*     */ 
/* 234 */       Network net2 = placeNetwork(0, 215);
/* 235 */       net2.setCIDR_IP(new IPAddress("2.0.0.0"), 8);
/* 236 */       Router router2 = placeRouter(32, 335);
/* 237 */       connectNetworkToRouter(net2, router2);
/* 238 */       connectNetworkToRouter(net2, router1);
/* 239 */       placeHost(net2, 150, 225);
/*     */ 
/* 241 */       Network net3 = placeNetwork(0, 430);
/* 242 */       net3.setCIDR_IP(new IPAddress("3.0.0.0"), 8);
/* 243 */       Router router3 = placeRouter(32, 550);
/* 244 */       connectNetworkToRouter(net3, router3);
/* 245 */       connectNetworkToRouter(net3, router2);
/* 246 */       placeHost(net3, 150, 438);
/*     */ 
/* 248 */       Network net4 = placeNetwork(202, 540);
/* 249 */       net4.setCIDR_IP(new IPAddress("4.0.0.0"), 8);
/* 250 */       Router router4 = placeRouter(412, 550);
/* 251 */       connectNetworkToRouter(net4, router4);
/* 252 */       connectNetworkToRouter(net4, router3);
/* 253 */       placeHost(net4, 242, 483);
/*     */ 
/* 255 */       Network net5 = placeNetwork(380, 430);
/* 256 */       net5.setCIDR_IP(new IPAddress("5.0.0.0"), 8);
/* 257 */       Router router5 = placeRouter(412, 335);
/* 258 */       connectNetworkToRouter(net5, router5);
/* 259 */       connectNetworkToRouter(net5, router4);
/* 260 */       placeHost(net5, 312, 438);
/*     */ 
/* 262 */       Network net6 = placeNetwork(380, 215);
/* 263 */       net6.setCIDR_IP(new IPAddress("6.0.0.0"), 8);
/* 264 */       Router router6 = placeRouter(412, 120);
/* 265 */       connectNetworkToRouter(net6, router6);
/* 266 */       connectNetworkToRouter(net6, router5);
/* 267 */       placeHost(net6, 312, 225);
/*     */ 
/* 269 */       Network net7 = placeNetwork(380, 0);
/* 270 */       net7.setCIDR_IP(new IPAddress("7.0.0.0"), 8);
/* 271 */       connectNetworkToRouter(net7, router6);
/* 272 */       placeHost(net7, 312, 12);
/*     */ 
/* 274 */       placeDatagram(241, 120);
/*     */ 
/* 276 */       this.board.repaint();
/* 277 */       JOptionPane.showMessageDialog(this.board, 
/* 278 */         "<html><b>Long</b> - This network is constructed of a list of routers and networks.<br><br>Routers are not configured.<br><br>You may add networks, hosts, routers, datagrams and configure them as you like.<br></html>");
/*     */     }
/*     */     catch (Throwable e)
/*     */     {
/* 285 */       return;
/*     */     }
/*     */   }
/*     */ 
/*     */   public Network placeNetwork(int x, int y) {
/* 290 */     x += 125;
/* 291 */     this.menuListener.setSelectedComponent(new MouseEvent(this.board, 0, 0L, 0, x, y, 1, false));
/* 292 */     this.menuListener.actionPerformed(new ActionEvent(this, 1001, "Add Network"));
/* 293 */     Network net = (Network)this.menuListener.getLastItem();
/* 294 */     net.setMTU(500);
/* 295 */     return net;
/*     */   }
/*     */ 
/*     */   public Router placeRouter(int x, int y) {
/* 299 */     x += 125;
/* 300 */     this.menuListener.setSelectedComponent(new MouseEvent(this.board, 0, 0L, 0, x, y, 1, false));
/* 301 */     this.menuListener.actionPerformed(new ActionEvent(this, 1001, "Add Router"));
/* 302 */     return (Router)this.menuListener.getLastItem();
/*     */   }
/*     */ 
/*     */   public Datagram placeDatagram(int x, int y) {
/* 306 */     x += 125;
/* 307 */     this.menuListener.setSelectedComponent(new MouseEvent(this.board, 0, 0L, 0, x, y, 1, false));
/* 308 */     this.menuListener.actionPerformed(new ActionEvent(this, 1001, "Add Datagram"));
/* 309 */     return (Datagram)this.menuListener.getLastItem();
/*     */   }
/*     */ 
/*     */   public Host placeHost(Network net, int x, int y) {
/* 313 */     x += 125;
/* 314 */     this.menuListener.setSelectedComponent(new MouseEvent(net, 0, 0L, 0, (int)net.getLocation().getX(), (int)net.getLocation().getY(), 1, false));
/* 315 */     this.menuListener.actionPerformed(new ActionEvent(net, 1001, "Add Host"));
/* 316 */     Host host = (Host)this.menuListener.getLastItem();
/* 317 */     host.setLocation(x, y);
/* 318 */     return host;
/*     */   }
/*     */ 
/*     */   public void connectNetworkToRouter(Network net, Router router) {
/* 322 */     router.networks.add(net);
/* 323 */     router.getIP(net);
/* 324 */     net.routers.add(router);
/* 325 */     if (net.defaultGateway == null)
/* 326 */       net.defaultGateway = router;
/*     */   }
/*     */ }

/* Location:           C:\Users\MT\Desktop\mtutr.jar
 * Qualified Name:     mtutr.listeners.NetworkPlacementListener
 * JD-Core Version:    0.5.4
 */
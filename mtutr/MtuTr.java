package mtutr;

import java.awt.Color;
import java.awt.Component;
import java.awt.Dimension;
import javax.swing.BoxLayout;
import javax.swing.ImageIcon;
import javax.swing.JApplet;
import javax.swing.JPanel;
import javax.swing.SwingUtilities;

public class MtuTr extends JApplet
{
  private static final long serialVersionUID = 1L;
  private JPanel jContentPane = null;
  private JPanel north = null;
  public boolean firstClick = true;
  public ImageIcon m = null;

  public void init()
  {
    setSize(750, 640);
    try
    {
      SwingUtilities.invokeAndWait(new Runnable() {
        public void run() {
          MtuTr.this.createGUI();
        } } );
    }
    catch (Exception e) {
      System.err.println("createGUI didn't successfully complete");
    }
  }

  public void createGUI() {
    setContentPane(getJContentPane());
  }

  private JPanel getJContentPane()
  {
    if (this.jContentPane == null) {
      this.jContentPane = new JPanel();
      this.jContentPane.setLayout(new BoxLayout(this.jContentPane, 1));

      this.north = new JPanel();
      this.north.setAlignmentY(0.0F);
      this.north.setAlignmentX(0.0F);
      this.north.setVisible(false);
      this.north.setBackground(new Color(255, 255, 128));
      this.north.setMaximumSize(new Dimension(getSize().width, 45));
      this.north.setPreferredSize(new Dimension(getSize().width, 45));

      JPanel south = new MainDrawBoard(this.north, this);

      if (this.firstClick) {
        this.firstClick = false;
      }

      south.setLayout(null);

      this.jContentPane.add(this.north);
      this.jContentPane.add(south);
    }
    return this.jContentPane;
  }

  public MainDrawBoard setNewMainBoard(Component comp) {
    this.jContentPane.remove(comp);
    MainDrawBoard south = new MainDrawBoard(this.north, this);
    MainDrawBoard.staticInstance = south;
    south.setLayout(null);
    south.setSize(comp.getSize());
    this.jContentPane.add(south);

    return south;
  }
}


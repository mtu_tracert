/*    */ package mtutr.common;
/*    */ 
/*    */ import mtutr.MtuTr;
import mtutr.MainDrawBoard;
/*    */ 
/*    */ public class BoardCleaner
/*    */   implements Runnable
/*    */ {
/*    */   private MtuTr kit;
/*    */   private MainDrawBoard board;
/*    */ 
/*    */   public BoardCleaner(MainDrawBoard _board)
/*    */   {
/* 11 */     this.kit = _board.ipKit;
/* 12 */     this.board = _board;
/*    */   }
/*    */ 
/*    */   public void run() {
/*    */     try {
/* 17 */       Thread.sleep(100L);
/*    */     }
/*    */     catch (InterruptedException e) {
/* 20 */       e.printStackTrace();
/*    */     }
/*    */ 
/* 23 */     this.kit.setNewMainBoard(this.board);
/* 24 */     this.kit.repaint();
/*    */   }
/*    */ }

/* Location:           C:\Users\MT\Desktop\mtutr.jar
 * Qualified Name:     mtutr.common.BoardCleaner
 * JD-Core Version:    0.5.4
 */
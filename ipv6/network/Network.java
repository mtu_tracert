package ipv6.network;

import ipv6.network.datagram.IDatagram;

import java.net.InetAddress;
import java.util.HashMap;
import java.util.Map;

public class Network extends BaseNetworkElement {

	private final Map<InetAddress, Host> hosts = new HashMap<InetAddress, Host>();
	private final Map<InetAddress, Router> routers = new HashMap<InetAddress, Router>();
	

	public void receiveDatagram(IDatagram datagram) {
		// TODO Auto-generated method stub
		
	}

	public void sendDatagram(IDatagram datagram) {
		Host host = hosts.get(datagram.getDestination().getInetAddress());
		if ( host != null){
			host.sendDatagram(datagram);
		} else {
			
		}
		
	}
}

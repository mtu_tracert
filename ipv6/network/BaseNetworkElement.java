package ipv6.network;

import java.net.InetAddress;

public abstract class BaseNetworkElement implements INetworkElement {

	private InetAddress inetAddress;
	private int mtu;

	public BaseNetworkElement() {
		super();
	}

	public InetAddress getInetAddress() {
		return inetAddress;
	}

	public void setInetAddress(InetAddress inetAddress) {
		this.inetAddress = inetAddress;
		
	}

	public int getMTU() {
		return mtu;
	}

	public void setMTU(int mtu) {
		this.mtu = mtu;
		
	}

}
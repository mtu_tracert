package ipv6.network.datagram;

import ipv6.network.Host;

public class Datagram implements IDatagram {

	private Host source;
	private Host destination;

	private int ttl;
	private boolean doNotFragment;
	private int size;

	
	
	public Datagram(Host source, Host destination, int ttl,
			boolean doNotFragment, int size) {
		super();
		this.source = source;
		this.destination = destination;
		this.ttl = ttl;
		this.doNotFragment = doNotFragment;
		this.size = size;
	}
	public Host getSource() {
		return source;
	}
	public void setSource(Host source) {
		this.source = source;
	}
	public Host getDestination() {
		return destination;
	}
	public void setDestination(Host destination) {
		this.destination = destination;
	}
	public int getTtl() {
		return ttl;
	}
	public void setTtl(int ttl) {
		this.ttl = ttl;
	}
	public boolean isDoNotFragment() {
		return doNotFragment;
	}
	public void setDoNotFragment(boolean doNotFragment) {
		this.doNotFragment = doNotFragment;
	}
	public int getSize() {
		return size;
	}
	public void setSize(int size) {
		this.size = size;
	}

}

package ipv6.network.datagram;

import ipv6.network.Host;


public class UDPDatagram extends Datagram {

	public UDPDatagram(Host source, Host destination, int ttl,
			boolean doNotFragment, int size) {
		super(source, destination, ttl, doNotFragment, size);
	}

}

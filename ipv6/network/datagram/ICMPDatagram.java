/**
 * 
 */
package ipv6.network.datagram;

import ipv6.network.Host;


/**
 * @author New user
 *
 */
public class ICMPDatagram extends Datagram {
	
	private int mtuSize;

	public ICMPDatagram(Host source, Host destination, int ttl,
			boolean doNotFragment, int size) {
		super(source, destination, ttl, doNotFragment, size);
	}

	public int getMtuSize() {
		return mtuSize;
	}

	public void setMtuSize(int mtuSize) {
		this.mtuSize = mtuSize;
	}


}

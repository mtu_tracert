package ipv6.network.datagram;

import ipv6.network.Host;

public interface IDatagram {

	public abstract Host getSource();

	public abstract void setSource(Host source);

	public abstract Host getDestination();

	public abstract void setDestination(Host destination);

	public abstract int getTtl();

	public abstract void setTtl(int ttl);

	public abstract boolean isDoNotFragment();

	public abstract void setDoNotFragment(boolean doNotFragment);

	public abstract int getSize();

	public abstract void setSize(int size);

}
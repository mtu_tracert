package ipv6.network.datagram;

public enum ICMPMessageType {
	
	ECHO_REQUEST,
	ECHO_REPLY,
	DESTINATION_UNREACHABLE,
	IPv6_PACKET_TOO_BIG

}

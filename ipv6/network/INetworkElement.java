package ipv6.network;

import ipv6.network.datagram.IDatagram;

import java.net.InetAddress;

public interface INetworkElement {
	public void sendDatagram(IDatagram datagram);
	public void receiveDatagram(IDatagram datagram);
	public InetAddress getInetAddress();
	public void setInetAddress(InetAddress inetAddress);
	
	public void setMTU(int mtu);
	public int getMTU();

}

package ipv6.network;

import ipv6.network.datagram.IDatagram;

public class NetworkSimulator {
	
	public void startSimulation(IDatagram datagram){
		
		Host sourceHost = datagram.getSource();
		sourceHost.sendDatagram(datagram);
		
	}

}

/**
 * 
 */
package ipv6.network;

import ipv6.network.datagram.IDatagram;


/**
 * @author New user
 *
 */
public class Host extends BaseNetworkElement {

	private Network network;

	/**
	 * 
	 */
	public Host() {
	}

	public void receiveDatagram(IDatagram datagram) {
		// TODO Auto-generated method stub
	}

	public void sendDatagram(IDatagram datagram) {
		if (datagram.getDestination().getInetAddress().equals(this.getInetAddress())){
			return;
		}
		network.sendDatagram(datagram);
	}

	public void setNetwork(Network network) {
		this.network = network;
	}

	public Network getNetwork() {
		return network;
	}

}
